![WebPyMail](https://gitlab.com/hgg/webpymail/-/raw/master/logos/webpymail-logo-banner.png?inline=true)

WebPyMail is a project that seeks to create a fully featured, [Python 3](https://docs.python.org/3/) and [Django](https://www.djangoproject.com/) based, webmail client.

Please note that I'm using Cyrus and Gmail to test, so if you try to use this on another kind of server the results may vary.

Bug reports and patches are welcome!

## Feature List

 * Same features as [squirrelmail](http://www.squirrelmail.org) without plugins (more or less):
    * **Folder list**:
        * [x] Subscribed folders;
        * [x] Expandable folder list;
        * [x] Read/Existing number of messsages;
        * [ ] Refresh folder list;
        * [ ] Subscribe/unsubscribe IMAP folders;
        * [ ] Create, rename, move and delete IMAP folders;
    * **Message List**:
        * [x] Paginated message list;
        * [x] Identify the server capability and use the SORT or THREAD command, fall back to a simple view for simple servers;
        * [x] Move messages;
        * [x] Copy messages;
        * [x] Mark message read;
        * [x] Mark message unread;
        * [x] Mark message deleted;
        * [x] Mark message undeleted;
        * [x] Show all messages;
        * [ ] Create interface for showing all messages (`page=all`);
    * **Message view**:
        * [x] Show the message TEXT/PLAIN part;
        * [x] Show the message TEXT/HTML part;
        * [ ] Ask the user for permission to see remote images (right now we have an all or nothing approach, system wide);
        * [ ] Maintain a list of allowed senders to display remote messages;
        * [x] Show encapsulated messages;
        * [x] Show attachments;
        * [x] Reply, Reply All;
        * [x] Forward, forward inline;
        * [x] Identify URLs and render them as links
        * Identify special message parts and display them accordingly:
            * [ ] S/MIME Cryptographic Signature (APPLICATION/PKCS7-SIGNATURE);
            * MULTIPART/REPORT:
                * [ ] MESSAGE/DELIVERY-STATUS;
        * Display special in-line elements and display them accordingly:
            * [ ] PGP signatures;
    * **Compose view**:
        * [x] Compose message in plain text;
        * [x] Compose message in Markdown;
        * [ ] Traditional message delivery status (`Disposition-Notification-To`);
        * Alternative message delivery status (is this ethical?):
            * [ ] Create a web bug to know if the message was seen, from where and when;
            * [ ] Display this info to the user;
        * [x] Add attachments;
        * [ ] Save message (as draft);
    * **Address book**:
        * [x] List and manage contacts (create, edit and delete);
        * [x] Create messages using the contacts;
        * [x] User, server and site level address books, the user can only create/edit/delete on the user level;
        * [ ] Permissions for users to change the address books at these levels;
        * [ ] Interface to give permissions;
        * [ ] Auto save new mail addresses;

* Other features:
    * [x] Multi server support;
    * Server admin interface (not Django's admin app):
        * [ ] Edit the configuration files;
        * [ ] Edit user permissions (address book permissions);
    * [x] IMAP authentication back end;
    * [x] Server list edited using the admin app;
    * [x] Auto user creation if successfully authenticated by the IMAP server;
    * [x] Authenticates always against the server, so no passwords on the db;
    * [x] BODYSTRUCTURE parser;

### Possible features

* SOHO features:
    * [ ] System wide signatures, enforceable by the webmaster;
    * [ ] Ability to disable user signatures;
    * [ ] Common pool of harvested mail addresses from all the accounts, if the user chooses to make the address public every user will have access to the mail address;
    * [ ] Support for LDAP address books (read and write);
    * [ ] Support carddav address books (read and write);
    * [ ] Support for IMAP ACLs, so that a user can share his folders;
    * Message templates:
        * [ ] Message templates (including custom css);
        * [ ] Message templates with forms;
        * [ ] Allow or disallow message templates for the user;
        * [ ] Force a message template to a user;
    * [ ] Database index of messages with the ENVELOPE and BODYSTRUCTURE info;
    * [ ] Sieve filter interface;
    * [ ] Permit plugins.

# History

This is not a new project, this project was started in 2008 but, due to several
reasons, the development was stopped for a while. It was hosted at [google
code](https://code.google.com/archive/p/webpymail/).

# License

WebPyMail is licensed under the terms of the GNU General Public License Version
3. See `LICENSE.txt` for details.

# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

# Global imports
import re
from typing import Generator, Union

Sexp = list[Union[bytes, 'Sexp']]


# Regexp
literal_re = re.compile(rb'^{(\d+)}\r\n')
simple_re = re.compile(rb'^([^ ()\[]+(?:\[[^\]]*\])?)')
quoted_re = re.compile(rb'^"((?:[^"\\]|\\")*?)"')
square_re = re.compile(rb'^(\[[^\]\[]*?\])')


# Errors


class SError(Exception):
    pass


def scan_sexp(data: bytes) -> Sexp:
    """
    S-Expression scanner

    Scan S-expressions in the provided data and return a nested list
    representation.

    This function processes a byte string containing S-expressions and converts
    it into a nested list structure (Sexp), where each list corresponds to an
    S-expression. The function handles quoted literals, numbered literals,
    square brackets, and simple literals, as well as nested S-expressions.

    Type hint:

    ```python
    Sexp = list[bytes | 'Sexp']
    ```

    Parameters
    ----------
    data
        The byte string containing S-expressions to be scanned.

    Returns
    -------
    Sexp
        A nested list structure representing the scanned S-expressions. Each
        S-expression is converted into a list, with nesting to represent
        sub-expressions.

    Raises
    ------
    SError
        If an unexpected parenthesis is encountered indicating unbalanced or
        malformed S-expressions.

    Notes
    -----
    The function assumes that the input data is properly formatted as per
    S-expression standards. It uses regular expressions to match different
    parts of the S-expressions and builds the nested list structure
    accordingly.

    Example:

    ```python
    >>> scan_sexp(b'(a (b c) "quoted string" {3}\\r\\nabc [square])')
    [b'a', [b'b', b'c'], b'quoted string', b'abc', b'square']
    ```
    """

    # Initialization
    pos = 0
    lenght = len(data)
    result: Sexp = []
    cur_result = result
    level = [cur_result]

    # Scanner
    while pos < lenght:
        # Quoted literal:
        if data[pos] == b'"'[0]:
            quoted = quoted_re.match(data[pos:])
            if quoted:
                cur_result.append(quoted.groups()[0])
                pos += quoted.end() - 1

        # Numbered literal:
        elif data[pos] == b'{'[0]:
            lit = literal_re.match(data[pos:])
            if lit:
                start = pos + lit.end()
                end = pos + lit.end() + int(lit.groups()[0])
                pos = end - 1
                cur_result.append(data[start:end])

        # Square brackets:
        elif data[pos] == b'['[0]:
            square = square_re.match(data[pos:])
            if square:
                cur_result.append(square.groups()[0])
                pos += square.end() - 1

        # Simple literal
        elif data[pos] not in b'() ':
            simple = simple_re.match(data[pos:])
            if simple:
                tmp = simple.groups()[0]
                if tmp == 'NIL':
                    tmp = None
                cur_result.append(tmp)
                pos += simple.end() - 1

        # Level handling, if we find a '(' we must add another list, if we
        # find a ')' we must return to the previous list.
        elif data[pos] == b'('[0]:
            cur_result.append([])
            cur_result = cur_result[-1]  # type: ignore
            level.append(cur_result)

        elif data[pos] == b')'[0]:
            try:
                cur_result = level[-2]
                del level[-1]
            except IndexError:
                raise SError('Unexpected parenthesis at pos %d' % pos)

        pos += 1

    return result


def flatten_sexp(nested_list: Sexp) -> Generator[bytes, None, None]:
    """
    Flatten a nested list.

    Parameters
    ----------
    nested_list
        The nested list to be flattened.

    Yields
    ------
    List[bytes]
        A generator that yields each element of the flattened list.

    Notes
    -----
    This function recursively flattens a nested list of bytes or nested lists
    of bytes. It yields each element of the flattened list in a depth-first
    traversal order.
    It ignores empty lists.

    Examples
    --------
    ```python
    >>> nested_list = [b'a', [b'b', [b'c', [], b'd'], b'e'], b'f']
    >>> flattened = list(flatten_sexp(nested_list))
    >>> flattened
    [b'a', b'b', b'c', b'd', b'e', b'f']
    ```
    """
    for item in nested_list:
        if isinstance(item, (list, tuple)):
            for sub_item in flatten_sexp(item):
                yield sub_item
        else:
            yield item


if __name__ == '__main__':
    raise Exception('See src/imaplib2/examples/sexp_example.py')

# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""
Utility functions and classes for the imaplib2 module.
"""

# Global imports
import binascii
import datetime
import re
from email.errors import HeaderParseError
from email.header import decode_header
from typing import Callable, Union

import pytz

from imaplib2.sexp import Sexp, flatten_sexp

# Utility functions


def get_unicode_header(header):
    """Returns an unicode string with the content of the
    header string.
    """
    if not header:
        return ''
    # Decode the header:
    header_list = []
    try:
        decoded_header = decode_header(header)
    except HeaderParseError:
        # This is necessary to deal with bug 22
        try:
            decoded_header = decode_header(header.replace('?==?', '?= =?'))
        except:
            # If unable to decode the header, return it unchanged
            return header
    for header in decoded_header:
        if not header[1]:
            codec = 'iso-8859-1'
        else:
            codec = header[1]
        if isinstance(header[0], str):
            # "decode_header" returns str if no charset is defined
            # imapll converts the bytes string to str using utf-8 encoding
            # if the server only returns ascii we should be OK in converting
            # back to bytes this way:
            txt = bytes(header[0], 'utf-8')
        else:
            # "decode_header" returns bytes if a charset is found:
            txt = header[0]
        try:
            text = str(txt, codec)
        except:
            try:
                text = str(txt, 'iso-8859-1')
            except:
                raise
        header_list.append(text)
    return ' '.join(header_list)


def get_unicode_mail_addr(address_list):
    """Return an address list with the mail addresses"""
    if not isinstance(address_list, list):
        return []

    # Verify the encoding:
    return [(unquote(get_unicode_header(Xi[0])), '%s@%s' % (Xi[2], Xi[3])) for Xi in address_list]


def integer_to_AP(num: int) -> bytes:
    """Convert integer to A-P bytes representation."""
    val = b''
    AP = b'ABCDEFGHIJKLMNOP'
    num = int(abs(num))
    while num:
        num, mod = divmod(num, 16)
        val = AP[mod : mod + 1] + val
    return val


def make_tagged(tagged):
    """Composes a string with the tagged response"""
    message_str = ' '.join(list_to_str(flatten_sexp(tagged['message'])))
    return f'Status: {tagged["status"]}\nMessage: {message_str}\nCommand: {tagged["command"]}'


def unquote(text: bytes) -> bytes:
    """Remove quotes from the text"""
    if len(text) > 0:
        if text[0] == text[-1] == b'"'[0] or text[0] == text[-1] == b"'"[0]:
            return text[1:-1]
    return text


MONTH_NUMBERS = {
    'Jan': 1,
    'Feb': 2,
    'Mar': 3,
    'Apr': 4,
    'May': 5,
    'Jun': 6,
    'Jul': 7,
    'Aug': 8,
    'Sep': 9,
    'Oct': 10,
    'Nov': 11,
    'Dec': 12,
}
# Month abbreviations
MONTH_ABBR = {value: key for key, value in MONTH_NUMBERS.items()}

internal_date_re = re.compile(
    r'(?P<day>[ 0123][0-9])-(?P<mon>[A-Z][a-z][a-z])-'
    r'(?P<year>[0-9][0-9][0-9][0-9])'
    r' (?P<hour>[0-9][0-9]):(?P<min>[0-9][0-9]):(?P<sec>[0-9][0-9])'
    r' (?P<zone>(?P<zonen>[-+])(?P<zoneh>[0-9][0-9])(?P<zonem>[0-9][0-9]))'
)

envelope_date_re = re.compile(
    r'(?:(?P<week_day>[A-Z][a-z][a-z]), )?'
    r'(?P<day>[ \d][0-9]?) '
    r'(?P<month>[A-Z][a-z][a-z]) '
    r'(?P<year>[0-9][0-9][0-9][0-9]) '
    r'(?P<hour>[0-9][0-9]):(?P<min>[0-9][0-9]):'
    r'(?P<sec>[0-9][0-9]) '
    r'(?P<zone>(?P<zonen>[-+])(?P<zoneh>[0-9][0-9])(?P<zonem>[0-9][0-9])).*'
)


def envelope_date_to_datetime(resp: str) -> datetime.datetime:
    """
    Convert an envelope date to tuple

    Returns Python time module tuple.

    Notes
    -----
    To show a localized date you can:
    ```python
    # If we have a time zone aware date:
    dt = datetime.datetime(2023, 4, 25, 16, 27, 0, tzinfo=pytz.utc)
    # Define the local time zone:
    lisbon = pytz.timezone('Europe/Lisbon')
    # Get the dt in the new time zone:
    dt_local = dt.astimezone(lisbon)
    ```
    """
    if not resp:
        return datetime.datetime.fromtimestamp(0)

    mo = envelope_date_re.match(resp)

    if not mo:
        return datetime.datetime.fromtimestamp(0)

    try:
        mon = MONTH_NUMBERS[mo.group('month')]
    except KeyError:
        return datetime.datetime.fromtimestamp(0)

    day = int(mo.group('day'))
    year = int(mo.group('year'))
    hour = int(mo.group('hour'))
    min = int(mo.group('min'))
    sec = int(mo.group('sec'))
    zone = mo.group('zone')

    # Redo the datetime string
    dt_str = f'{year}-{mon:0>2}-{day:0>2} {hour:0>2}:{min:0>2}:{sec:0>2} {zone}'

    # Create a datetime object in the received time zone
    lt = datetime.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S %z')

    # Return datetime in the UTC time zone
    return lt.astimezone(pytz.utc)


def internal_date_to_datetime(resp: str) -> datetime.datetime:
    """
    Convert IMAP4 INTERNALDATE to UT.

    Returns Python datetime.datetime object in the UTC time zone.

    Notes
    -----
    To show a localized date you can:
    ```python
    # If we have a time zone aware date:
    dt = datetime.datetime(2023, 4, 25, 16, 27, 0, tzinfo=pytz.utc)
    # Define the local time zone:
    lisbon = pytz.timezone('Europe/Lisbon')
    # Get the dt in the new time zone:
    dt_local = dt.astimezone(lisbon)
    ```
    """
    mo = internal_date_re.match(resp)
    if not mo:
        return None

    mon = MONTH_NUMBERS[mo.group('mon')]

    day = int(mo.group('day'))
    year = int(mo.group('year'))
    hour = int(mo.group('hour'))
    min = int(mo.group('min'))
    sec = int(mo.group('sec'))
    zone = mo.group('zone')

    # Redo the datetime string
    dt_str = f'{year}-{mon:0>2}-{day:0>2} {hour:0>2}:{min:0>2}:{sec:0>2} {zone}'

    # Create a datetime object in the received time zone
    lt = datetime.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S %z')

    # Return datetime in the UTC time zone
    return lt.astimezone(pytz.utc)


def list_to_int(msg_list: Sexp) -> list[int]:
    """
    Converts a message list, nested or not, to integers.

    Parameters
    ----------
    msg_list
        The message list to be converted. Can be a nested list containing
        string representations of integers or direct string representations
        of integers.

    Returns
    -------
    list[int]
        A new list where each element is converted to an integer. If an
        element is a nested list, it is recursively converted to integers.

    Notes
    -----
    This function iterates through the input list and recursively converts
    each element to an integer using either `int()` or by recursively
    calling itself for nested lists.

    Examples
    --------
    >>> list_to_int(['1', '2', '3'])
    [1, 2, 3]

    >>> list_to_int([['1', '2'], ['3', '4']])
    [[1, 2], [3, 4]]
    """
    return [list_to_int(msg_id) if isinstance(msg_id, list) else int(msg_id) for msg_id in msg_list]


def datetime_imap(dt: datetime.datetime, encoding: bytes = 'utf-8') -> bytes:
    """
    Converts a datetime object to the format used by the IMAP protocol
    """
    # Use MONTH_ABBR to ensure we use the right month abbreviation the %b
    # directive show the month as locale’s abbreviated name
    month = MONTH_ABBR[dt.month]
    if dt.tzinfo is not None and dt.tzinfo.utcoffset(dt) is not None:
        dt_buf = dt.strftime(f'"%d-{month}-%Y %H:%M:%S %z"').encode(encoding)
    else:
        dt_buf = dt.strftime(f'"%d-{month}-%Y %H:%M:%S"').encode(encoding)
    return dt_buf


def list_to_str(msg_list: Sexp, encoding: str = 'utf-8') -> list[str]:
    """Converts a message list of bytes to a list of str, nested or not."""
    return [
        list_to_str(el, encoding) if isinstance(el, list) else el.decode(encoding)
        for el in msg_list
    ]


def list_to_bytes(msg_list: list[str], encoding: str = 'utf-8') -> list[bytes]:
    """Converts a message list of strings to a list of bytes, nested or not."""
    return [
        list_to_bytes(el, encoding) if isinstance(el, list) else el.encode(encoding)
        for el in msg_list
    ]


def shrink_fetch_list(msg_list: list[int]) -> list[bytes]:
    """
    Shrinks the message list to use on the fetch command, consecutive msg_list
    numbers will be converted to first:last.

    Parameters
    ----------
    msg_list
        A list of message numbers or uids

    Returns
    -------
    list[bytes]
        A list with the shrinked msg_list
    """
    tmp: list[bytes] = []
    msg_list = list(msg_list)
    if not msg_list:
        return []

    msg_list.sort()
    last = msg_list[0]
    anchor: int = 0

    for msgn in msg_list[1:]:
        if (last + 1) == msgn and not anchor:
            anchor = last
        elif anchor:
            if (last + 1) != msgn:
                if (anchor + 1) != last:
                    tmp.append(b'%d:%d' % (anchor, last))
                else:
                    tmp.append(b'%d' % anchor)
                    tmp.append(b'%d' % last)
                anchor = 0
        else:
            tmp.append(b'%d' % last)

        last = msgn

    if anchor:
        if (anchor + 1) != last:
            tmp.append(b'%d:%d' % (anchor, last))
        else:
            tmp.append(b'%d' % anchor)
            tmp.append(b'%d' % last)
    else:
        tmp.append(b'%d' % last)

    return tmp


class UTF7Error(Exception):
    pass


def encode_to_utf7(st: str) -> bytes:
    """
    Encode string to modified utf-7.

    From the RFC3501:

    In modified UTF-7, printable US-ASCII characters, except for "&", represent
    themselves; that is, characters with octet values 0x20-0x25 and 0x27-0x7e.
    The character "&" (0x26) is represented by the two-octet sequence "&-".

    All other characters (octet values 0x00-0x1f and 0x7f-0xff) are represented
    in modified BASE64, with a further modification from RFC2152 that "," is
    used instead of "/".  Modified BASE64 MUST NOT be used to represent any
    printing US-ASCII character which can represent itself.

    Parameters
    ----------
    st
        String to convert

    Returns
    -------
    bytes
        The string converted to utf-7

    Notes
    -----
    * RFC3501: [Mailbox International Naming Convention](https://datatracker.ietf.org/doc/html/rfc3501#section-5.1.3)
    * RFC2152: [UTF-7 A Mail-Safe Transformation Format of Unicode](https://datatracker.ietf.org/doc/html/rfc2152)
    """
    result = b''
    tmp_st = ''
    idx = 0
    lenght = len(st)
    while idx < lenght:
        ch = st[idx]
        if (0x20 <= ord(ch) <= 0x25 or 0x27 <= ord(ch) <= 0x7E) and not tmp_st:
            result += ord(ch).to_bytes()
        elif ch == '&' and not tmp_st:
            result += b'&-'
        elif (0x20 <= ord(ch) <= 0x25 or 0x27 <= ord(ch) <= 0x7E or ch == '&') and tmp_st:
            tmp_utf16 = tmp_st.encode('utf-16be')
            tmp_base64 = binascii.b2a_base64(tmp_utf16).rstrip(b'\n=').replace(b'/', b',')
            result += b'&' + tmp_base64 + b'-'
            tmp_st = ''
            continue
        else:
            tmp_st += ch
        idx += 1
    return result


VALIDBASE64 = b'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,'


def decode_utf7(buf: bytes) -> str:
    """
    Decode modified utf-7 to str.

    Please see [utils.encode_to_utf7][imaplib2.utils.encode_to_utf7].

    Parameters
    ----------
    buf
        Bytes buffer to be converted to str

    Returns
    -------
    The converted str
    """
    result = ''
    tmp_buf = b''
    idx = 0
    lenght = len(buf)
    b64 = False
    while idx < lenght:
        ch = buf[idx]
        if (0x20 <= ch <= 0x25 or 0x27 <= ch <= 0x7E) and not b64:
            result += chr(ch)
        elif ch == ord('&'):
            if idx + 1 < lenght:
                if buf[idx + 1] == ord('-'):
                    result += '&'
                    idx += 1
                else:
                    b64 = True
            else:
                raise UTF7Error("Can't end modified utf-7 with &")
        elif b64 and ch != ord('-') and ch in VALIDBASE64:
            tmp_buf += bytes(chr(ch), 'ascii')
        elif b64 and ch == ord('-'):
            result += (b'+' + tmp_buf.replace(b',', b'/') + b'-').decode('utf-7')
            b64 = False
            tmp_buf = b''
        else:
            raise UTF7Error('Invalid base64, character %s not allowed.' % chr(ch))
        idx += 1
    if b64:
        raise UTF7Error('Base64 set of characters not ended correctly.')
    return result


#
# Classes
#


class ContinuationRequests(list):
    """
    Manage continuation requests made by the server.
    """

    def push(self, data: Union[bytes | Callable]) -> None:
        """
        Push data to the front of the continuation requests list.

        Parameters
        ----------
        data
            The data to be added to the front of the continuation requests
            list. If it's a bytes object, it will be popped unmodified when the
            next continuation is requested by the server. If it's a callable,
            the callable will be called with the continuation data (the
            challenge) as an argument, and the return value will be sent to the
            server.
        """
        self.insert(0, data)

    def pop(self, challenge: bytes) -> bytes:
        """
        Pop the next continuation response from the continuation requests list.

        Parameters
        ----------
        challenge
            The challenge data sent by the server.

        Returns
        -------
        bytes
            The response to the server's continuation request.

        Notes
        -----
        If the continuation requests list is empty, this method returns b'*' as a default response.
        """
        try:
            response = list.pop(self)
            try:
                # Check if the response is a callable
                response = response(challenge)
            except TypeError:
                pass
            return response
        except IndexError:  # Empty list
            return b'*'

    def clear(self) -> None:
        """
        Clear all items from the continuation requests list.
        """
        del self[:]

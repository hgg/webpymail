# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""IMAP command list and their allowed states"""

# From imaplib module:
COMMANDS = {
    # name            # valid states
    b'APPEND': ('AUTH', 'SELECTED'),
    b'AUTHENTICATE': ('NONAUTH',),
    b'CAPABILITY': ('NONAUTH', 'AUTH', 'SELECTED', 'LOGOUT'),
    b'CHECK': ('SELECTED',),
    b'CLOSE': ('SELECTED',),
    b'COPY': ('SELECTED',),
    b'CREATE': ('AUTH', 'SELECTED'),
    b'DELETEACL': ('AUTH', 'SELECTED'),
    b'DELETE': ('AUTH', 'SELECTED'),
    b'EXAMINE': ('AUTH', 'SELECTED'),
    b'EXPUNGE': ('SELECTED',),
    b'FETCH': ('SELECTED',),
    b'GETACL': ('AUTH', 'SELECTED'),
    b'GETANNOTATION': ('AUTH', 'SELECTED'),
    b'GETQUOTA': ('AUTH', 'SELECTED'),
    b'GETQUOTAROOT': ('AUTH', 'SELECTED'),
    b'LIST': ('AUTH', 'SELECTED'),
    b'LISTRIGHTS': ('AUTH', 'SELECTED'),
    b'LOGIN': ('NONAUTH',),
    b'LOGOUT': ('NONAUTH', 'AUTH', 'SELECTED', 'LOGOUT'),
    b'LSUB': ('AUTH', 'SELECTED'),
    b'MYRIGHTS': ('AUTH', 'SELECTED'),
    b'NAMESPACE': ('AUTH', 'SELECTED'),
    b'NOOP': ('NONAUTH', 'AUTH', 'SELECTED', 'LOGOUT'),
    b'PARTIAL': ('SELECTED',),  # NB: obsolete
    b'PROXYAUTH': ('AUTH',),
    b'RENAME': ('AUTH', 'SELECTED'),
    b'SEARCH': ('SELECTED',),
    b'SELECT': ('AUTH', 'SELECTED'),
    b'SETACL': ('AUTH', 'SELECTED'),
    b'SETANNOTATION': ('AUTH', 'SELECTED'),
    b'SETQUOTA': ('AUTH', 'SELECTED'),
    b'SORT': ('SELECTED',),
    b'STATUS': ('AUTH', 'SELECTED'),
    b'STORE': ('SELECTED',),
    b'SUBSCRIBE': ('AUTH', 'SELECTED'),
    b'THREAD': ('SELECTED',),
    b'UID': ('SELECTED',),
    b'UNSELECT': ('SELECTED',),
    b'UNSUBSCRIBE': ('AUTH', 'SELECTED'),
}

# Addicional status messages returned on the tagged or untagged responses:
# 'OK','NO','BAD', 'PREAUTH', 'BYE'
STATUS = (
    'ALERT',
    'BADCHARSET',
    'CAPABILITY',
    'PARSE',
    'PERMANENTFLAGS',
    'READ-ONLY',
    'READ-WRITE',
    'TRYCREATE',
    'UIDNEXT',
    'UIDVALIDITY',
    'UNSEEN',
    'MAILBOXID',
    'COPYUID',
    # b'APPENDUID', # RFC 2359 - IMAP4 UIDPLUS extension
)

# Fetch responses
FETCHRESP = (
    b'BODY',
    rb'BODY\[(?P<section>.*?)\]<(?P<origin>\d*)>',
    b'BODYSTRUCTURE',
    b'ENVELOPE',
    b'FLAGS',
    b'INTERNALDATE',
    b'RFC822',
    b'RFC822.HEADER',
    b'RFC822.SIZE',
    b'RFC822.TEXT',
    b'UID',
)

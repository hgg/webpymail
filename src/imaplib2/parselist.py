# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""
This module contains the Mailbox class used for parsing the IMAP LIST command
response.

The LIST command in IMAP protocol retrieves the mailbox names from the mail
server. This module provides the Mailbox class to represent and manipulate
these mailbox names.

Classes
-------
Mailbox
    A class representing a mailbox, with methods to access its properties and
    compare it with other mailboxes.

Attributes
----------
NOSELECT : bytes
    A byte string representing the '\\Noselect' attribute from the LIST
    command.
HASCHILDREN : bytes
    A byte string representing the '\\HasChildren' attribute from the LIST
    command.
HASNOCHILDREN : bytes
    A byte string representing the '\\HasNoChildren' attribute from the LIST
    command.

Examples
--------

```python
>>> from imaplib2.parselist import Mailbox, NOSELECT, HASCHILDREN
>>> mailbox = Mailbox(b'INBOX', [NOSELECT, HASCHILDREN], b'/')
>>> mailbox.noselect()
True
>>> mailbox.has_children()
True
>>> mailbox == b'INBOX'
True
>>> str(mailbox)
'INBOX'
>>> mailbox.level()
0
>>> mailbox.last_level()
b'INBOX'
>>> mailbox.native()
b'INBOX'
>>> mailbox.url()
b'SU5CT1g='
```
"""

# Imports

import base64
from typing import List

from imaplib2.utils import decode_utf7, encode_to_utf7

# Attributes:

NOSELECT = r'\Noselect'
HASCHILDREN = r'\HasChildren'
HASNOCHILDREN = r'\HasNoChildren'


class Mailbox:
    def __init__(self, path: bytes, attributes: List[bytes], delimiter: bytes) -> None:
        """
        Initialize a Mailbox object.

        Parameters
        ----------
        path
            The path of the mailbox.

        attributes
            List of attributes associated with the mailbox.

        delimiter
            The delimiter used to separate levels in the mailbox hierarchy.

        Returns
        -------
        None

        Notes
        -----
        - The `path` parameter represents the name or path of the mailbox.
        - The `attributes` parameter is a list containing bytes representing
        various attributes of the mailbox.
        - The `delimiter` parameter is a single byte representing the delimiter
        used to separate levels in the mailbox hierarchy.
        - The `parts` attribute is initialized based on the `path` and
        `delimiter`, splitting the path into different parts if a delimiter is
        provided.
        """
        self.path = decode_utf7(path)
        self.delimiter = delimiter.decode('utf-8')
        self.attributes = tuple(attributes)

        if delimiter:
            self.parts = tuple(self.path.split(self.delimiter))
        else:
            self.parts = (self.path,)

    # Attributes
    def test_attribute(self, attr: str) -> bool:
        """
        Check if a specific attribute is present in the list of attributes
        associated with the mailbox.

        Parameters
        ----------
        attr
            The attribute to be checked.

        Returns
        -------
        bool
            True if the attribute is present, False otherwise.
        """
        return attr in self.attributes

    def noselect(self) -> bool:
        """
        Check if the mailbox has the \\Noselect attribute.

        Returns
        -------
        bool
            True if the \\Noselect attribute is present, False otherwise.
        """
        return self.test_attribute(NOSELECT)

    def has_children(self) -> bool:
        """
        Check if the mailbox has the \\HasChildren attribute.

        Returns
        -------
        bool
            True if the \\HasChildren attribute is present, False otherwise.
        """
        return self.test_attribute(HASCHILDREN)

    # Operators
    def __eq__(self, mb: object) -> bool:
        """
        Compare the mailbox name against a string or another mailbox object.

        Parameters
        ----------
        mb
            The object to compare against.

        Returns
        -------
        bool
            True if the mailbox name matches the string or mailbox object, False otherwise.
        """
        if isinstance(mb, Mailbox):
            return self.path == mb.path
        elif isinstance(mb, str):
            mailbox = self.path
            if mailbox.upper() == 'INBOX':
                # INBOX should be case insensitive
                mailbox = 'INBOX'
                mb = mb.upper()
            return mb == mailbox
        return False

    def __str__(self) -> str:
        """
        Return a string representation of the mailbox.

        Returns
        -------
        str
            A string representation of the mailbox.
        """
        if self.path.upper() == 'INBOX':
            # INBOX should be case insensitive
            return 'INBOX'
        return self.path

    # Mailbox name
    def level(self) -> int:
        """
        Get the level of the mailbox in the hierarchy.

        Returns
        -------
        int
            The level of the mailbox.
        """
        return len(self.parts) - 1

    def last_level(self) -> str:
        """
        Get the last level of the mailbox.

        Returns
        -------
        bytes
            The last level of the mailbox.
        """
        return self.parts[-1]

    def native(self) -> str:
        """
        Get the mailbox in raw format using the delimiter understood by the
        server.

        Returns
        -------
        str
            The mailbox in raw format.
        """
        return self.path

    def url(self) -> str:
        """
        Get the folder name in a URL-safe way.

        Returns
        -------
        bytes
            The folder name encoded in a URL-safe format.
        """
        return base64.urlsafe_b64encode(encode_to_utf7(self.path)).decode('utf-8')

    def __repr__(self) -> str:
        """
        Return a string representation of the Mailbox object.

        Returns
        -------
        str
            A string representation of the Mailbox object.
        """
        return '<Mailbox instance "%s">' % self.path

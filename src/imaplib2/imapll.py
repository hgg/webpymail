# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with imaplib2.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""
This is an IMAP low level module, it provides the basic mechanisms to connect
to an IMAP server. The server responses are tokenized into s-expressions and
classified into tagged or untagged response. No further processing is made.

NOTE: This code is an adaptation of the original [][imaplib] module code (the
standard python IMAP client module) by Piers Lauder.
"""

# Global imports
import random
import re
import socket
import ssl
from typing import Callable, Optional, Tuple

from imaplib2.sexp import Sexp, flatten_sexp, scan_sexp

# Local imports
from imaplib2.utils import ContinuationRequests, integer_to_AP

# Types

# Response = dict[str, dict[str, bytes] | list]
TaggedResponse = dict[str, Sexp | bytes]

# Constants

D_SERVER = 1  #: Debug responses from the server
D_CLIENT = 2  #: Debug data sent by the client
D_RESPONSE = 4  #: Debug obtained response

Debug = 0
# Debug = D_SERVER | D_CLIENT | D_RESPONSE

MAXCOMLEN = 48  #: Max command len to store on the tagged_commands dict

IMAP4_PORT = 143  #: Default IMAP port
IMAP4_SSL_PORT = 993  # : Default IMAP SSL port
CRLF = b'\r\n'

literal_re = re.compile(rb'.*{(?P<size>\d+)}$')
send_literal_re = re.compile(rb'.*{(?P<size>\d+)}\r\n')


class IMAP4:
    """
    Bare bones IMAP client.

    This class implements a very simple IMAP client, all it does is to send
    strings to the server and retrieve the server responses.

    Features:

    * The literals sent from the server are treated the same way as any other
      element. For instance, if we request an envelope from a message, the
      server can represent the subject as a literal. The envelope response will
      be assembled as a single string and then transformed into a s-exp;
    * The continuation requests are handled transparently with the help of the
      [ContinuationRequests][imaplib2.utils.ContinuationRequests];
    * The responses are encapsulated on a dictionary.

    For this conversation::

    ```
    C: OBOC0001 LOGOUT<cr><lf>
    S: * BYE LOGOUT received<cr><lf>
    S: OBOC0001 OK Completed<cr><lf>
    ```

    We get::

    ```python
    {'untagged': [b'*', b'BYE', b'LOGOUT', b'received'],
     'tagged': {b'OBOC0001': { 'status': b'OK',
                               'message': [b'Completed'],
                               'tag': b'OBOC0001',
                               'command': b'LOGOUT'
    }}}
    ```

    It's very easy to transform this class so that we can send severall
    commands to the server in paralel. Maybe in the future we can implement
    this.

    The calling structure when sending a command is:

    ```
    send_command
    │
    ├── _new_tag
    │
    ├── send(command)
    │   │
    │   └── sock.sendall(data)
    │
    └── return tag, self.read_responses(tag)
        │
        ├── scan_sexp(_get_line())
        │   │
        │   └── readline()
        │       │
        │       └── file.readline()
        │
        └── parse_command(tag, response)
    ```

    Attributes:

    * host (str): The hostname of the IMAP server;
    * port (int): The port number to connect to (default is IMAP4_PORT);
    * tagged_commands (dict): Dictionary to store tagged commands sent to the server;
    * continuation_data (ContinuationRequests): Queue to handle continuation requests;
    * state (str): State of the connection ('LOGOUT', 'AUTH', or 'NONAUTH');
    * welcome (Sexp): Server response upon connection.

    Usage example:

    ```python
    from imaplib2.imapll import IMAP4

    Debug = D_SERVER | D_CLIENT | D_RESPONSE

    M = IMAP4( host )
    tag, response = M.send_command(b'LOGIN %s "%s"' %('user', 'some pass'))
    tag, response = M.send_command(b'CAPABILITY')
    tag, response = M.send_command(b'LOGOUT' )
    ```
    """

    class Error(Exception):
        """Logical errors - debug required"""

    class Abort(Exception):
        """Service errors - close and retry"""

    class ReadOnly(Exception):
        """Mailbox status changed to READ-ONLY"""

    def __init__(
        self, host: str, port: int = IMAP4_PORT, parse_command: Optional[Callable] = None
    ) -> None:
        """
        Initialize the IMAP4 client:

        Parameters
        ----------
        host
            The hostname of the IMAP server
        port
            The port number to connect to (default is IMAP4_PORT)
        parse_command
            Function to parse server responses (default is None).

        Raises
        ------
        IMAP4.Error
            If an unexpected response is received upon connection.

        """

        # Connection
        self.host = host
        self.port = port

        # Create unique tag for this session,
        # and compile tagged response matcher.
        self.tagpre = integer_to_AP(random.randint(4096, 65535))
        self.tagre = re.compile(rb'(?P<tag>' + self.tagpre + rb'\d+)')
        self.tagnum = 0

        self.tagged_commands: dict[bytes, bytes] = {}
        self.continuation_data = ContinuationRequests()
        self.encoding = 'utf-8'

        # Open the connection to the server
        self.open(host, port)

        # State of the connection:
        self.state = 'LOGOUT'

        self.welcome = scan_sexp(self._get_line())

        if parse_command:
            self.parse_command = parse_command
        else:
            self.parse_command = self.dummy_parse_command

        if b'PREAUTH' in self.welcome:
            self.state = 'AUTH'
        elif b'OK' in self.welcome:
            self.state = 'NONAUTH'
        else:
            raise self.Error(self.welcome)

    ##
    # Overridable methods
    ##

    def open(self, host: str = 'localhost', port: int = IMAP4_PORT) -> None:
        """
        Setup connection to remote server on "host:port".

        This connection (self.file) will be used by the routines: read,
        readline, send, shutdown.

        Parameters
        ----------
        host
            Hostname to connect to. Defaults to 'localhost'.
        port
            Port to connect to. Defaults to IMAP4_PORT.
        """
        self.host = host
        self.port = port

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))
        self.file = self.sock.makefile('rb')

    def read(self, size: int) -> bytes:
        """
        Read 'size' bytes from the remote server.

        Parameters
        ----------
        size
            The number of bytes to read from the server.

        Returns
        -------
        bytes
            The bytes read from the server.
        """
        if __debug__:
            if Debug & D_SERVER:
                print('S: Read %d bytes from the server.' % size)
        return self.file.read(size)

    def readline(self) -> bytes:
        """
        Read a line from the remote server.

        Returns
        -------
        bytes
            The line read from the server.
        """
        line: bytes = self.file.readline()
        if not line:
            raise self.Abort('socket error: EOF')
        if __debug__:
            if Debug & D_SERVER:
                print('S: %r' % line)
        return line

    def send(self, data: bytes) -> None:
        """
        Send data to the remote server.

        Parameters
        ----------
        data
            The data to send to the server.
        """
        if __debug__:
            if Debug & D_CLIENT:
                print('C: %r' % data)
        try:
            self.sock.sendall(data)
        except OSError as val:
            raise self.Abort('socket error: %s' % val)

    def shutdown(self) -> None:
        """
        Close I/O established in "open".
        """
        self.file.close()
        self.sock.close()

    def socket(self) -> socket.socket:
        """
        Return socket instance used to connect to IMAP4 server.

        Returns
        -------
        socket.socket
            The socket instance used for the connection.
        """
        return self.sock

    def push_continuation(self, obj: bytes | Callable) -> None:
        """
        Insert a continuation in the continuation queue.

        Parameters
        ----------
        obj : bytes | Callable
            The continuation data to insert into the queue.
            If a bytes string, it will be popped unmodified when the next continuation
            is requested by the server. If it's a callable, the return from the
            callable will be sent to the server instead. The callable is called using the
            continuation data as an argument.
        """
        self.continuation_data.push(obj)

    ##
    # SEND/RECEIVE commands from the server
    ##

    def send_command(self, command: bytes, read_resp: bool = True) -> Tuple[bytes, dict]:
        """
        Send a command to the server.

        * Handles literals sent to the server;
        * Updates the tags sent to the server (IMAP4.tagged_commands);
        * IMAP4.tagged_commands[tag] - contains the first MAXCOMLEN of the
          command.

        Parameters
        ----------
        command
            The command to send to the server, without the tag and the final
            CRLF.
        read_resp
            If True, automatically reads the server response. Defaults to True.

        Returns
        -------
        Tuple[bytes, dict]
            If read_resp is True, returns a tuple containing the tag used on
            the sent command and the server response. Otherwise, returns only
            the tag and an empty dictionary.
        """
        tag = self._new_tag()

        # Do not store the complete command on tagged_commands
        if len(command) > MAXCOMLEN:
            tagcommand = command[:MAXCOMLEN] + b' ...'
        else:
            tagcommand = command

        # Check for a literal:
        lt = send_literal_re.search(command)
        if lt:
            # If there are any additional command arguments, the literal octets
            # are followed by a space and those arguments (from RFC3501 sec 7.5)
            self.continuation_data.push(command[lt.end() :])
            command = command[: lt.end() - 2]

        # Send the command to the server
        self.tagged_commands[tag] = tagcommand
        command_literal = b'%s %s\r\n' % (tag, command)
        self.send(command_literal)

        if read_resp:
            return tag, self.read_responses(tag)
        else:
            return tag, {}

    def read_responses(self, command_tag: bytes) -> dict:
        """
        Reads the responses from the server.

        The rules followed are:

        * If the line starts with a `"*" + SP` it's an untagged response;
        * If the line ends with `{<number>}CRLF` it's an IMAP literal and on the
          same iteration of the loop the next `number` bytes from the server
          will be read;
        * If the line starts with `<tag> + SP` it's the end of a tagged
          response.

        The returned data is in the form::

        ```python
        response = {
            'tagged' : {
                TAG001: {
                    'status': ...,
                    'message': ...,
                    'command': ... },
                    ...
                    },
            'untagged' : [
                '* 1st untagged',
                '* 2nd untagged',
                ...
                ]
            }
        ```

        Parameters
        ----------
        command_tag
            The tag to read the response for. Please note that due the IMAP
            characteristics we can't predict the server response order. Because
            of this, it's possible to have in a single response several tagged
            responses besides the tag we are asking the response for. In any
            case this method will stop reading from the server as soon as it
            has read the tagged response for the tag parameter.

        Returns
        -------
        dict
            The server response filtered by parse_command.
        """
        response: dict = {'tagged': {}, 'untagged': []}

        while self.tagged_commands:
            # If we have responses to read we should get them from the server
            # up until there are no more responses.
            # Read a line from the server:
            line = scan_sexp(self._get_line())
            flat_line = list(flatten_sexp(line))
            tag = flat_line[0]

            # Now the server responses are broadly classified. The possibilities are:
            #
            # * It's a tagged response, the response will be encapsulated on a dict;
            # * It's an untagged response, we return a list of tokens returned by scan_sexp;
            # * It's a continuation request, '+ <continuation data>CRLF', a
            #   continuation response will be popped from the continuation
            #   queue. If we don't have a prepared continuation, we'll try to
            #   cancel the command by sending a '*'. (see the
            #   ContinuationRequests class).
            if tag.startswith(self.tagpre):
                # It's a tagged response
                if tag not in self.tagged_commands:
                    raise self.Abort('unexpected tagged response: %s' % tag.decode('utf-8'))
                tagged_response = {
                    'status': line[1],
                    'message': line[2:],
                    'tag': line[0],
                    'command': self.tagged_commands[tag],
                }
                response['tagged'][tag] = tagged_response
                del self.tagged_commands[tag]
            elif tag == b'*':
                # It's an untagged response
                response['untagged'].append(line)
            elif tag == b'+':
                # It's a continuation, we're sending a literal
                self.send(self.continuation_data.pop(b' '.join(flat_line[1:])) + b'\r\n')
            else:
                raise self.Abort("Don't know how to handle:\nS: %r" % b' '.join(flat_line))

        self.continuation_data.clear()

        if __debug__:
            if Debug & D_RESPONSE:
                print(response)

        return self.parse_command(command_tag, response)

    def dummy_parse_command(self, tag: bytes, response: dict) -> dict:
        """
        Further processing of the server response.

        This method is called by [IMAP4.read_responses][imaplib2.imapll.IMAP4.read_responses].

        Parameters
        ----------
        tag
            The tag used on the command.
        response
            A server response. Check
            [IMAP4.read_responses][imaplib2.imapll.IMAP4.read_responses] for
            the response structure.

        Returns
        -------
        dict
            The response, unmodified.
        """
        return response

    ##
    # Private methods
    ##

    def _new_tag(self) -> bytes:
        """Returns a new tag."""
        tag = b'%s%03d' % (self.tagpre, self.tagnum)
        self.tagnum += 1
        return tag

    def _get_line(self) -> bytes:
        """Gets a line from the server. If the line contains a literal in it,
        it will recurse until we have read a complete line.
        """
        # Read a line from the server
        line = self.readline()[:-2]

        # Verify if a literal is comming
        lt = literal_re.match(line)
        if lt:
            # read 'size' bytes from the server and append them to
            # the line read and read the rest of the line
            size = int(lt.group('size'))
            literal = self.read(size)
            line += CRLF + literal + self._get_line()
        return line


class IMAP4_SSL(IMAP4):
    """IMAP4 client class over SSL connection"""

    def __init__(self, host, port=IMAP4_SSL_PORT, keyfile=None, certfile=None, parse_command=None):
        self.keyfile = keyfile
        self.certfile = certfile
        IMAP4.__init__(self, host=host, port=port, parse_command=parse_command)

    def open(self, host='', port=IMAP4_SSL_PORT):
        """Setup connection to remote server on "host:port".
            (default: localhost:standard IMAP4 SSL port).
        This connection will be used by the routines:
            read, readline, send, shutdown.
        """
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))
        self.sslobj = ssl.wrap_socket(self.sock, self.keyfile, self.certfile)

    def read(self, size):
        """Read 'size' bytes from remote."""
        if __debug__:
            if Debug & D_SERVER:
                print('S: Read %d bytes from the server.' % size)
        # sslobj.read() sometimes returns < size bytes
        chunks = []
        read = 0
        while read < size:
            data = self.sslobj.read(size - read)
            read += len(data)
            chunks.append(data)
        return b''.join(chunks)

    def readline(self):
        """Read line from remote."""
        # NB: socket.ssl needs a "readline" method, or perhaps a "makefile"
        # method.
        line = []
        while 1:
            char = self.sslobj.read(1)
            line.append(char)
            if char == b'\n' or len(char) == 0:
                if __debug__:
                    if Debug & D_SERVER:
                        print('S: %r' % b''.join(line))
                return b''.join(line)

    def send(self, data):
        """Send data to remote."""
        if __debug__:
            if Debug & D_CLIENT:
                print('C: %r' % data)
        len_bytes = len(data)
        while len_bytes > 0:
            sent = self.sslobj.write(data)
            if sent == len_bytes:
                break  # avoid copy
            data = data[sent:]
            len_bytes = len_bytes - sent

    def shutdown(self):
        """Close I/O established in "open"."""
        self.sock.close()

    def socket(self):
        """Return socket instance used to connect to IMAP4 server.

        socket = <instance>.socket()
        """
        return self.sock

    def ssl(self):
        """Return SSLObject instance used to communicate with the IMAP4 server.

        ssl = <instance>.socket.ssl()
        """
        return self.sslobj


if __name__ == '__main__':
    raise Exception('See src/imaplib2/examples/imapll_example.py')

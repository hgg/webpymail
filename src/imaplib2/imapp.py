# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""
This is an IMAP parsed library. The final objective is to have a library which
interacts with an IMAP server and parses to python structures all the server
responses.

Since the server will respond with multiple untagged responses to a given
command, each untagged response will update the `IMAP4P.sstatus` with the
appropriate data which will be return at the command end.
"""

# Global imports
import re
import socket
import ssl
from datetime import datetime
from typing import Any, Callable, Dict, List, Literal, Optional, TypedDict, Union

from imaplib2 import parselist

# Local imports
from imaplib2.imapcommands import COMMANDS, STATUS
from imaplib2.imapll import IMAP4, IMAP4_SSL
from imaplib2.infolog import InfoLog
from imaplib2.parsefetch import FetchParser
from imaplib2.sexp import Sexp, scan_sexp
from imaplib2.utils import (
    datetime_imap,
    decode_utf7,
    encode_to_utf7,
    list_to_bytes,
    list_to_int,
    list_to_str,
    make_tagged,
    shrink_fetch_list,
    unquote,
)

# Constants
D_NOTPARSED = 8
D_DEL = 16
Debug = D_NOTPARSED | D_DEL
Debug = 0
IMAP4_PORT = 143
IMAP4_SSL_PORT = 993
MAXLOG = 100
CRLF = b'\r\n'
SP = ' '
MAXCLILEN = 16384  # max command line length accepted by the IMAP server

# Regexp
opt_respcode_re = re.compile(rb'^\[(?P<code>[a-zA-Z0-9-]+)(?P<args>.*?)\].*$')
response_re = re.compile(rb'^(?P<code>[a-zA-Z0-9-]+)(?P<args>.*)$', re.MULTILINE)
fetch_data_items_re = re.compile(rb'^([a-zA-Z0-9\[\]<>\.]+) ')
fetch_flags_re = re.compile(rb'^\((.*?)\) ?')
fetch_int_re = re.compile(rb'^(\d+) ?')
fetch_quoted_re = re.compile(rb'^"(.*?)" ?')
map_crlf_re = re.compile(rb'\r\n|\r|\n')

##
# Types
##

ResponseType = tuple[Literal['OK', 'NO'], str, Any]


class ServerStatusType(TypedDict):
    """
    Server Status Type

    This dictionary is used to store the server responses.
    """

    acl_response: Dict
    capability: list[str]
    copy_response: tuple[str, ...]
    create_response: Dict[str, str]
    current_folder: Dict
    fetch_response: Dict[int, FetchParser]
    list_response: list[parselist.Mailbox]
    listrights_response: Dict
    myrights_response: Dict
    namespace: dict
    search_response: tuple
    sort_response: tuple
    status_response: Dict
    thread_response: list[int]


#   'acl_response': {'mailbox': '',
#                    'acl': {}},
#   'listrights_response': {'mailbox': '',
#                           'identifier': '',
#                           'req_rights': '',
#                           'opt_rights': () },
#   'myrights_response': {'mailbox': '',
#                         'rights': '' },
#   'current_folder': {
#       'name': str,
#       'is_readonly': bool,
#       'PERMANENTFLAGS': (),
#       'UNSEEN': int,
#       'UIDVALIDITY': int,
#       'UIDNEXT': int,
#       'HIGHESTMODSEQ': int,
#       'ANNOTATIONS': int,
#       'MAILBOXID': str,
#       'URLMECH': str,
#       }


class IMAP4P:
    """
    This class implements an IMAP client.

    It is based on the [imaplib][imaplib] python standard library.

    This class is used by calling its methods that correspond to IMAP commands
    and also some utility methods.  The calling structure is:

    ```
    Call a Command
    │
    └── _process_command(name, args)
        │
        ├── _test_command(name)
        │
        └── send_command(command)  # imapll
            │
            ├── _parse_command(tag, response)
            │   │
            │   ├── _parse_tagged(tag, tagged_responses)
            │   │   └── _parse_optional_codes(tagged_responses)
            │   │
            │   └── _parse_untagged(tag, untagged_response)
            │        └── Command response
            │
            └── _checkok(tag, response)
    ```

    The state of the client is stored in `IMAP4P.sstatus`, this is a
    dictionary. When a command returns it can return this dictionary or part of
    it. For instance the search command will return the list of message
    sequence numbers (or UIDs).

    For instance, when we select a mailbox on the server, the related responses
    are stored in `IMAP4P.sstatus['current_folder']`. If we close this
    mailbox `IMAP4P.sstatus['current_folder']` reverts to `{}`.

    Besides putting the server responses on this structure, other useful
    information is also stored there. For instance, under 'current_folder' we
    also put the folder name which is not present on the server responses.

    A log of the client activity is also kept in `IMAP4P.infolog`, this is an
    instance of [InfoLog][imaplib2.infolog.InfoLog], this can be redefined to
    suit our needs. InfoLog implements a simple callback mechanism. We can add
    specific actions to be performed when a certain type of log is made.

    The IMAP commands implemented on this class follow RFC3501 unless otherwise
    noted.

    TODO: Think about transforming the `IMAP4P.sstatus` so that we can also
          define callbacks when certain values change on this structure.

    TODO: Describe IMAP4P.state

    The `IMAP4P.sstatus` dict has the following empty state::

    ```python
    {
    'capability' : (),
    'sort_response': {},
    'list_response': list[Mailbox],
    'search_response': (),
    'sort_response': (),
    'status_response': {},
    'fetch_response': Dict[int, FetchParser],
    'acl_response': {'mailbox': '',
                     'acl': {}},
    'listrights_response': {'mailbox': '',
                            'identifier': '',
                            'req_rights': '',
                            'opt_rights': () },
    'myrights_response': {'mailbox': '',
                          'rights': '' },
    'namespace': {
        'personal': [],
        'other_users': [],
        'shared': [],
    },
    'current_folder': {
        'name': str,
        'is_readonly': bool,
        'PERMANENTFLAGS': (),
        'UNSEEN': int,
        'UIDVALIDITY': int,
        'UIDNEXT': int,
        'HIGHESTMODSEQ': int,
        'ANNOTATIONS': int,
        'MAILBOXID': str,
        'URLMECH': str,
        }
    }
    ```

    Please note the when the object is destroyed we do an automatic logout,
    you can still use the logout method, but in that case you should
    override the `__del__` method, else you're going to raise an exception
    when you try to logout from a closed connection when the object is
    deleted.
    """

    class Error(Exception):
        """Logical errors - debug required"""

    class Abort(Exception):
        """Service errors - close and retry"""

    class ReadOnly(Exception):
        """Mailbox status changed to READ-ONLY"""

    sstatus: ServerStatusType

    def __init__(
        self,
        host,
        port=None,
        ssl=False,
        keyfile=None,
        certfile=None,
        infolog=InfoLog(MAXLOG),
        autologout=True,
    ):
        """
        Initialize a connection to an IMAP server.

        Parameters
        ----------
        host : str
            The hostname or IP address of the IMAP server.
        port : int, optional
            The port to use when connecting to the server. Defaults to 143 for
            non-SSL connections, or 993 for SSL connections.
        ssl : bool, optional
            If True, create an SSL connection. Otherwise, create a non-SSL
            connection. Defaults to False.
        keyfile : str, optional
            The path to the private key file for SSL connections. Defaults to
            None.
        certfile : str, optional
            The path to the certificate file for SSL connections. Defaults to
            None.
        infolog : InfoLog, optional
            An instance of InfoLog for logging information. Defaults to
            InfoLog(MAXLOG).
        autologout : bool, optional
            If True, automatically log out from the server when the object is
            deleted. Defaults to True.

        Examples
        --------
        ```python
        >>> imap_conn = IMAPClient('imap.example.com', ssl=True)
        >>> imap_conn = IMAPClient('imap.example.com', port=993, ssl=True, keyfile='/path/to/keyfile', certfile='/path/to/certfile')
        ```
        """

        # Choose the right connection, and then connect to the server
        self.autologout = autologout
        if not port:
            if ssl:
                port = IMAP4_SSL_PORT
            else:
                port = IMAP4_PORT

        try:
            if ssl:
                self.__IMAP4 = IMAP4_SSL(
                    host=host,
                    port=port,
                    keyfile=keyfile,
                    certfile=certfile,
                    parse_command=self._parse_command,
                )
            else:
                self.__IMAP4 = IMAP4(host=host, port=port, parse_command=self._parse_command)
            self.connected = True
        except socket.gaierror:
            self.connected = False
            raise

        # Wrap IMAP4
        self.welcome = self.__IMAP4.welcome
        self.send_command = self.__IMAP4.send_command
        self.state = self.__IMAP4.state
        self.shutdown = self.__IMAP4.shutdown
        self.push_continuation = self.__IMAP4.push_continuation
        self.encoding = self.__IMAP4.encoding

        # Server status
        self.sstatus = {
            'acl_response': {'mailbox': '', 'acl': {}},
            'capability': (),
            'copy_response': (),
            'create_response': {},
            'current_folder': {
                'name': '',
                'is_readonly': True,
                'PERMANENTFLAGS': (),
                'UNSEEN': 0,
                'UIDVALIDITY': 0,
                'UIDNEXT': 0,
                'HIGHESTMODSEQ': 0,
                'ANNOTATIONS': 0,
                'MAILBOXID': '',
                'URLMECH': '',
            },
            'fetch_response': {},
            'list_response': [],
            'listrights_response': {},
            'myrights_response': {},
            'namespace': {},
            'search_response': (),
            'sort_response': (),
            'status_response': {},
            'thread_response': (),
        }

        # Status messages from the server
        self.infolog = infolog
        self.infolog.add_entry('WELCOME', self.welcome)
        self._parse_optional_codes(self.welcome)

        self.capabilities = self.sstatus['capability']

    def __del__(self):
        if __debug__:
            if Debug & D_DEL:
                print('Deleting ImapServer instance.')
        if self.autologout and self.connected:
            self.logout()
            self.shutdown()

    ##
    # Command processing and response parsing
    ##

    # Call structure
    #
    # Call a Command
    # │
    # └── _process_command(name, args)
    #     │
    #     ├── _test_command(name)
    #     │
    #     └── send_command(command)  # imapll
    #         │
    #         ├── _parse_command(tag, response)
    #         │   │
    #         │   ├── _parse_tagged(tag, tagged_responses)
    #         │   │   └── _parse_optional_codes(tagged_responses)
    #         │   │
    #         │   └── _parse_untagged(tag, untagged_response)
    #         │        └── Command response
    #         │
    #         └── _checkok(tag, response)

    def _process_command(
        self, name: bytes, args: Optional[bytes] = None
    ) -> tuple[Literal['OK', 'NO'], str]:
        """
        Processes the current IMAP command:

        1. checks is the command is valid;
        2. composes the command from its name and arguments;
        3. send the command to the server and get the response (for this we use imapll);
        4. checks if the command was successful and parses the response (when
           initializing IMAP4 from imapll we define the parse_command callback
           as self._parse_command);
        5. if yes, return the server status dictionary;
        7. if not raise self.Error.

        Parameters
        ----------
        name
            Valid IMAP4 command.
        args
            Command arguments.

        Returns
        -------
        IMAP4P.sstatus
            Status information after processing the command.

        Raises
        ------
        IMAP4P.Error
            If there's an error in processing the command.
        """
        # Verifies if it's a valid command
        self._test_command(name)

        # Composes the command
        if args:
            command = b'%s %s' % (name, args)
        else:
            command = name

        # Sends the command to the server, and parses the response
        tag, response = self.send_command(command)

        # FIX: For the moment we have no way of getting the tagged response
        #      messages. We must find a way of passing this information to the
        #      user.

        # Checks if the command was successful
        if self._checkok(tag, response):
            # Successful command
            return 'OK', tag.decode(self.encoding)
        elif self._checkno(tag, response):
            # Unsuccessful command
            return 'NO', tag.decode(self.encoding)
        else:
            message = b' '.join(response['tagged'][tag]['message']).decode(self.encoding)
            raise self.Error('Error in command %s - %s' % (name.decode(self.encoding), message))

    def _process_command_UID(self, name: bytes, args: bytes) -> tuple[Literal['OK', 'NO'], str]:
        """
        Processes IMAP commands using the UID alternatives. The difference to
        `self.process_command` is that the UID command will make part of the
        composed command.

        Parameters
        ----------
        name
            The name of the IMAP command.
        args
            Arguments for the command.

        Returns
        -------
        IMAP4P.sstatus
            Status information after processing the command.

        Raises
        ------
        IMAP4P.Error
            If there's an error in processing the command.
        """
        self._test_command(b'UID')
        self._test_command(name)

        command = b'UID %s %s' % (name, args)

        # Sends the command to the server, and parses the response
        tag, response = self.send_command(command)

        # Checks if the command was successful
        if self._checkok(tag, response):
            # Successful command
            return 'OK', tag.decode(self.encoding)
        elif self._checkno(tag, response):
            # Unsuccessful command
            return 'NO', tag.decode(self.encoding)
        else:
            message = b' '.join(response['tagged'][tag]['message']).decode(self.encoding)
            raise self.Error('Error in UID command %s - %s' % (name.decode(self.encoding), message))

    def _test_command(self, name: bytes) -> None:
        """
        Tests if a given command is legal in the current state.

        Parameters
        ----------
        name
            The name of the IMAP command.

        Raises
        ------
        IMAP4P.Error
            If the command is not legal in the current state.
        """
        if self.state not in COMMANDS[name]:
            raise self.Error('command %r illegal in state %r' % (name, self.state))

    def _parse_command(self, tag: bytes, response: dict) -> dict:
        """
        Further processes the server response. The tagged and untagged
        responses are processed separately.

        Parameters
        ----------
        tag
            The tag of the server response.
        response
            The server response.

        Returns
        -------
        dict
            The processed response.
        """
        self._parse_tagged(tag, response['tagged'])
        self._parse_untagged(tag, response['untagged'])

        return response

    def _parse_tagged(self, tag: bytes, tagged_responses: dict) -> None:
        """
        Handles tagged responses from the server.

        The status of the response can be: OK - the command was successful, NO
        - we have a server operational error or BAD - there was a protocol
        server error. Additionally the tagged response can have optional codes
        between square brackets, that part of the response is handled in
        `self._parse_optional_codes`.

        Parameters
        ----------
        tag
            The tag of the server response.
        tagged_responses
            Tagged responses from the server.

        Raises
        ------
        Error
            If there's a protocol-level or bad response status.
        """
        for tag in tagged_responses:
            tagged = tagged_responses[tag]
            status = tagged['status'].upper()
            command = tagged['command'].split()[0].decode(self.encoding)
            if status in (b'OK', b'NO', b'BAD'):
                # NO - Server operational error, the command failed
                # BAD - Protocol level error
                # Update the server info log
                self.infolog.add_entry(status, tagged)
                if status == b'BAD':
                    raise self.Error(
                        'Protocol-level error, check the command '
                        'syntaxe. \n%s' % (make_tagged(tagged))
                    )
            else:
                raise self.Error(
                    'Bad response status from ' 'the server. \n%s' % (make_tagged(tagged))
                )

            # Check if there are optional response codes:
            self._parse_optional_codes(tagged['message'], command)

    def _parse_optional_codes(self, message: list, command: Optional[str] = None) -> None:
        """
        Parses optional codes from a server message.

        This method handles the optional codes present in tagged responses and
        also the untagged responses OK and BYE.

        Parameters
        ----------
        message
            The message containing optional codes.
        command
            If we're parsing an optional code originating from a tagged
            response this argument will identify the command used.

        Raises
        ------
        IMAP4P.Error
            If there's an unknown optional code.
        """
        optional_codes = None
        for token in message:
            if token[0] == b'['[0] and token[-1] == b']'[-1]:
                optional_codes = scan_sexp(token[1:-1])
        if not optional_codes:
            return

        if isinstance(optional_codes[0], bytes):
            code = optional_codes[0].upper().decode()
        else:
            raise self.Error('Got a list instead of a bytes sequence: %r' % optional_codes[0])

        if len(optional_codes) == 2:
            args = optional_codes[1]
        elif len(optional_codes) > 2:
            args = optional_codes[1:]
        else:
            args = None

        if code not in STATUS:
            if __debug__:
                if Debug & D_NOTPARSED:
                    print("Don't know how to handle optional code:\n%s" % message)
            return  # Silently ignore unknown OPTIONAL codes

        # Atoms
        if code == 'READ-ONLY':
            self.sstatus['current_folder']['is_readonly'] = True
        elif code == 'READ-WRITE':
            self.sstatus['current_folder']['is_readonly'] = False
        elif code in ('ALERT', 'TRYCREATE', 'PARSE'):
            self.infolog.add_entry(code, message)
        elif code == 'CAPABILITY':
            if isinstance(args, list):
                self._CAPABILITY_response(code, args)
            else:
                raise self.Error(f'Expected list of capabilities got {args!r}.')
        elif code == 'PERMANENTFLAGS':
            # Parenthesized list
            if isinstance(args, list):
                self.sstatus['current_folder'][code] = tuple(
                    item.decode('utf-8') for item in args if isinstance(item, bytes)
                )
            else:
                raise self.Error(f'Expected list of flags got {args!r}.')
        elif code == 'COPYUID':
            # Parenthesized list
            if isinstance(args, list):
                self.sstatus['copy_response'] = tuple(
                    item.decode('utf-8') for item in args if isinstance(item, bytes)
                )
            else:
                raise self.Error(f'Expected list of flags got {args!r}.')
        elif code in ('UNSEEN', 'UIDVALIDITY', 'UIDNEXT', 'HIGHESTMODSEQ', 'ANNOTATIONS'):
            # Integer responses:
            if isinstance(args, bytes):
                self.sstatus['current_folder'][code] = int(args)
            else:
                raise self.Error(f'Expected an int to {code}, got {args}.')
        elif code == 'MAILBOXID':
            if isinstance(args, list) and isinstance(args[0], bytes):
                if command == 'CREATE':
                    self.sstatus['create_response'][code] = args[0].decode('utf-8')
                else:
                    self.sstatus['current_folder'][code] = args[0].decode('utf-8')
            else:
                raise self.Error(f'Expected a bytes sequence to {code}, got {args!r}.')
        elif code == 'URLMECH':
            if isinstance(args, bytes):
                self.sstatus['current_folder'][code] = args.decode('utf-8')
            else:
                raise self.Error(f'Expected a bytes sequence to {code}, got {args!r}.')
        else:
            raise self.Error(f"Don't know how to parse  {code} - {args!r}")

    def _parse_untagged(self, tag: bytes, untagged_response: list) -> None:
        """
        Handles untagged responses from the server.

        Some commands have more complex responses that must be handled
        elsewhere. To do this method will call, if it exists, the method named:
        `_<command name>_response`.

        Parameters
        ----------
        tag
            The tag of the server response.
        untagged_response
            Untagged responses from the server.
        """
        for untagged in untagged_response:
            # get the response type
            if not (untagged[1].isalnum() or untagged[1].isalpha()):
                raise self.Error('Parse error: %s' % untagged)
            code = untagged[1].upper()
            args = untagged[2:]

            # Some responses come with an integer at the beginning, if that's the case, we
            # switch the order of this response
            try:
                int(code)
                code, args = untagged[2].upper(), [code] + untagged[3:]
            except ValueError:
                pass

            code_str = code.decode('utf-8')
            # Call handler function based on the response type
            method_name = '_' + code_str.replace('.', '_') + '_response'
            method = getattr(self, method_name, self._default_response)
            method(code_str, args)

            # TODO: Here we could emit the appropriate signals to a
            # controller

    def _default_response(self, code: str, args: Sexp) -> None:
        """
        Handles default responses from the server. By default it does nothing.

        Parameters
        ----------
        code
            The response code.
        args
            The arguments associated with the response.
        """
        if __debug__:
            if Debug & D_NOTPARSED:
                print("Don't know how to handle:\n * %s %s" % (code, args))

    def _checkok(self, tag: bytes, response: dict) -> bool:
        """
        Checks if the command was successful by checking if the response status
        is OK.

        Parameters
        ----------
        tag
            The tag of the server response.
        response
            The server response.

        Returns
        -------
        bool
            True if the command was successful, False otherwise.
        """
        return response['tagged'][tag]['status'] == b'OK'

    def _checkno(self, tag: bytes, response: dict) -> bool:
        """
        Checks if the command was unsuccessful by checking if the response status
        is NO.

        Parameters
        ----------
        tag
            The tag of the server response.
        response
            The server response.

        Returns
        -------
        bool
            True if the command was unsuccessful, False otherwise.
        """
        return response['tagged'][tag]['status'] == b'NO'

    # Auxiliary methods

    def _CRAM_MD5_AUTH(self, challenge: bytes) -> bytes:
        """
        Generate response for CRAM-MD5 authentication challenge.

        This method generates a response to a CRAM-MD5 authentication challenge
        received from the IMAP server. It computes the HMAC hash of the
        password using the challenge as the key and concatenates the username
        and the computed hash to form the response.

        Parameters
        ----------
        challenge
            The challenge string received from the IMAP server.

        Returns
        -------
        bytes
            The response string to be sent to the server for authentication.
        """
        import hmac

        response = (
            self.user + b' ' + hmac.HMAC(self.password, challenge).hexdigest().encode(self.encoding)
        )

        del self.user
        del self.password
        return response

    def _fetch(
        self, uid: bool, message_list: List[int], message_parts: str = '(FLAGS)'
    ) -> tuple[Literal['OK', 'NO'], str, Dict[int, FetchParser]]:
        """Fetch (parts of) messages"""
        # Convert arguments to bytes
        message_parts_buf = message_parts.encode(self.encoding)

        if uid:
            process_command = self._process_command_UID
        else:
            process_command = self._process_command

        name = b'FETCH'

        self.sstatus['fetch_response'] = {}

        if not message_list:
            raise self.Error("Can't fetch an empty message list.")
        if not isinstance(message_list, list):
            raise self.Error('The message list must be an integer list')

        # The message list can be rather long sometimes. Each
        # IMAP server has a maximum length for the command line
        # so if the command line is bigger than a MAXCLILEN we
        # have to make several fetch commands to complete the
        # fetch.

        shrinked_list = shrink_fetch_list(message_list)
        message_list_part = b','.join(shrinked_list)

        # Worst case scenario command length
        command_len = len(b'UID FETCH %s %s' % (message_list_part, message_parts_buf)) + 2

        if command_len > MAXCLILEN:
            shrinked_list = list(shrinked_list)
            len_overhead = len(b'UID FETCH %s' % (message_parts_buf)) + 2
            message_list_part = b''
            result = {}
            while shrinked_list:
                msg = shrinked_list.pop()
                if len(message_list_part) + len(msg) + len_overhead <= MAXCLILEN:
                    # Compose the partial message list
                    message_list_part += b',%s' % msg
                else:
                    # Make a partial fetch
                    shrinked_list.append(msg)
                    args = b'%s %s' % (message_list_part.strip(b','), message_parts_buf)
                    status, tag = process_command(name, args)
                    tmp_result = self.sstatus['fetch_response']

                    # Merge the results:
                    for key in tmp_result:
                        result[key] = tmp_result[key]
                    message_list_part = b''

            if message_list_part:
                args = b'%s %s' % (message_list_part.strip(b','), message_parts_buf)
                status, tag = process_command(name, args)
                tmp_result = self.sstatus['fetch_response']
                for key in tmp_result:
                    result[key] = tmp_result[key]

            self.sstatus['fetch_response'] = result
            return status, tag, result

        args = b'%s %s' % (message_list_part, message_parts_buf)
        status, tag = process_command(name, args)
        return status, tag, self.sstatus['fetch_response']

    ##
    # Specific command response handling
    ##

    def _BYE_response(self, code: str, args: Sexp) -> None:
        self._parse_optional_codes(args)
        self.infolog.add_entry(code, args)

    def _CAPABILITY_response(self, code: str, args: Sexp) -> None:
        self.sstatus['capability'] = [
            cb.decode(self.encoding).upper() for cb in args if isinstance(cb, bytes)
        ]

    def _EXISTS_response(self, code: str, args: Sexp) -> None:
        if isinstance(args, list) and isinstance(args[0], bytes):
            self.sstatus['current_folder']['EXISTS'] = int(args[0])
        else:
            raise self.Error('Error in command processing EXISTS response.')

    def _FETCH_response(self, code: str, args: Sexp) -> None:
        # Message number
        try:
            if isinstance(args[0], bytes):
                msg_num = int(args[0])
            else:
                raise self.Error('Expected bytes to convert to integer.')
        except ValueError:
            raise self.Error('Error in command processing FETCH response.')

        # Parse the response:
        response = FetchParser(args[1])  # type: ignore
        response['ID'] = msg_num
        if 'UID' in response:
            # If UID command, index by uid
            self.sstatus['fetch_response'][response['UID']] = response
        else:
            self.sstatus['fetch_response'][msg_num] = response

    def _FLAGS_response(self, code: str, args: Sexp) -> None:
        self.sstatus['current_folder'][code] = tuple(
            flag.decode('utf-8') for flag in args[0] if isinstance(flag, bytes)
        )

    def _ACL_response(self, code: str, args: Sexp) -> None:
        acl = self.sstatus['acl_response']['acl']
        if isinstance(args[1], bytes) and isinstance(args[2], bytes):
            acl[args[1].decode(self.encoding)] = args[2].decode(self.encoding)

    def _LIST_response(self, code: str, args: Sexp) -> None:
        if isinstance(args[0], list):
            attributes = args[0]
        else:
            raise self.Error('Error processing attributes of the LIST response.')
        if isinstance(args[1], bytes):
            hierarchy_delimiter = args[1]
        else:
            raise self.Error('Error processing the delimiter of the LIST response.')
        if isinstance(args[2], bytes):
            name = args[2]
        else:
            raise self.Error('Error processing the mailbox path of the LIST response.')

        # If the hierarchy_delimiter is NIL no hierarchy exists
        if hierarchy_delimiter != 'NIL':
            hierarchy_delimiter = unquote(hierarchy_delimiter)
        else:
            hierarchy_delimiter = b''

        self.sstatus['list_response'].append(
            parselist.Mailbox(
                name,
                attributes,  # type: ignore
                hierarchy_delimiter,
            )
        )

    _LSUB_response = _LIST_response

    def _LISTRIGHTS_response(self, code: str, args: Sexp) -> None:
        self.sstatus['listrights_response'] = {
            'mailbox': decode_utf7(args[0]) if isinstance(args[0], bytes) else None,
            'identifier': args[1].decode(self.encoding) if isinstance(args[1], bytes) else None,
            'req_rights': args[2].decode(self.encoding) if isinstance(args[2], bytes) else None,
            'opt_rights': tuple(
                right.decode(self.encoding) for right in args[2:] if isinstance(right, bytes)
            ),
        }

    def _MYRIGHTS_response(self, code: str, args: Sexp) -> None:
        self.sstatus['myrights_response'] = {
            'mailbox': decode_utf7(args[0]) if isinstance(args[0], bytes) else None,
            'rights': args[1].decode(self.encoding) if isinstance(args[1], bytes) else None,
        }

    def _NAMESPACE_response(self, code: str, args: Sexp) -> None:
        self.sstatus['namespace']['personal'] = (
            list_to_str(args[0]) if isinstance(args[0], list) else None
        )
        self.sstatus['namespace']['other_users'] = (
            list_to_str(args[1]) if isinstance(args[1], list) else None
        )
        self.sstatus['namespace']['shared'] = (
            list_to_str(args[2]) if isinstance(args[2], list) else None
        )

    def _OK_response(self, code: str, args: Sexp) -> None:
        self._parse_optional_codes(args)

    def _RECENT_response(self, code: str, args: Sexp) -> None:
        if isinstance(args, list) and isinstance(args[0], bytes):
            self.sstatus['current_folder']['RECENT'] = int(args[0])

    def _SEARCH_response(self, code: str, args: Sexp) -> None:
        self.sstatus['search_response'] = tuple(int(xi) for xi in args if isinstance(xi, bytes))

    def _SORT_response(self, code: str, args: Sexp) -> None:
        self.sstatus['sort_response'] = tuple(int(xi) for xi in args if isinstance(xi, bytes))

    def _THREAD_response(self, code: str, args: Sexp) -> None:
        self.sstatus['thread_response'] = list_to_int(args) if isinstance(args, list) else None

    def _STATUS_response(self, code: str, args: Sexp) -> None:
        args_str = list_to_str(args[1]) if isinstance(args[1], list) else None
        if isinstance(args_str, list):
            it = iter(args_str)

        self.sstatus['status_response'] = dict(list(zip(it, it)))
        # Try to convert to int:
        for key, value in self.sstatus['status_response'].items():
            try:
                self.sstatus['status_response'][key] = int(value)
            except ValueError:
                pass
        self.sstatus['status_response']['mailbox'] = (
            decode_utf7(args[0]) if isinstance(args[0], bytes) else None
        )

    ##
    # IMAP Commands
    ##

    # Utility methods

    def has_capability(self, capability: str) -> bool:
        """
        Check if the server supports a given capability.

        Parameters
        ----------
        capability
            The capability to test for.

        Returns
        -------
        bool
            True if the server supports the capability; False otherwise.
        """
        if not self.capabilities:
            self.capabilities = self.capability()

        return capability.upper() in self.capabilities

    def has_uid(self) -> bool:
        """
        Since it's not clear from the capability that we're testing for UID we
        have this method.
        """
        return self.has_capability('IMAP4REV1')

    # IMAP Commands

    def append(
        self,
        mailbox: str,
        message: bytes,
        flags: Optional[list[str]] = None,
        date_time: Optional[datetime] = None,
    ) -> ResponseType:
        """
        Append a new message to a specific mailbox on the IMAP server.

        This method allows you to append a new message to the specified mailbox
        on the IMAP server.

        Parameters
        ----------
        mailbox
            The name of the mailbox to append the message to.

        message
            The message to append, formatted as a string in RFC 822 format.

        flags
            A list of flags to set on the appended message. Each flag should be
            specified as a string, such as '\\Seen' or '\\Draft'. Defaults to
            None, indicating no flags are set.

        date_time
            The date and time of the message. This should be provided as a
            datetime object. Defaults to None, in which case the current date
            and time are used.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAPError
            If an error occurs during the execution of the APPEND command, such
            as permission denied or mailbox not found.

        Notes
        -----
        * Client Commands - Authenticated State;
        * The `mailbox` parameter specifies the name of the mailbox to which
          the message will be appended;
        * The `message` parameter should be a string formatted according to RFC
          822 standards;
        * The `flags` parameter allows you to set one or more flags on the
          appended message. These flags control various aspects of the
          message's behavior, such as its visibility or status;
        * The `date_time` parameter allows you to specify the date and time of
          the message. If not provided, the current date and time are used.
        * RFC3501: [APPEND Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.11)

        Examples
        --------
        ```python
        >>> client.append(b'INBOX', b'Subject: Test\\r\\n\\r\\nThis is a test message.')
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)
        if flags:
            flags_buf = [flag.encode(self.encoding) for flag in flags]
        else:
            flags_buf = None

        # Assemble and send the append command
        name = b'APPEND'

        # Use CRLF as line ending
        message = map_crlf_re.sub(CRLF, message)
        # Create a literal
        message = b'{%d}%s%s' % (len(message), CRLF, message)

        aux_args: List[bytes] = []

        if flags_buf:
            aux_args.append(b'(' + b' '.join(flags_buf) + b')')

        if date_time:
            aux_args.append(datetime_imap(date_time, self.encoding))

        aux_args_st = b' '.join(aux_args)

        if aux_args:
            args = b'"%s" %s %s' % (mailbox_buf, aux_args_st, message)
        else:
            args = b'"%s" %s' % (mailbox_buf, message)

        status, tag = self._process_command(name, args)
        return status, tag, None

    def authenticate(
        self, mech: bytes, authobject: Union[bytes, list[bytes], Callable]
    ) -> ResponseType:
        """
        Send an AUTHENTICATE command to the server.

        The AUTHENTICATE command indicates a SASL - Simple Authentication and
        Security Layer - authentication mechanism to the server. If the server
        supports the requested authentication mechanism, it performs an
        authentication protocol exchange to authenticate and identify the
        client. It MAY also negotiate an OPTIONAL security layer for subsequent
        protocol interactions. If the requested authentication mechanism is not
        supported, the server SHOULD reject the AUTHENTICATE command by sending
        a tagged NO response.

        Parameters
        ----------
        mech
            The authentication mechanism to use. This should be specified as a
            bytes object.

        authobject
            The authentication object or a list of authentication objects. The
            format of the authentication object(s) depends on the
            authentication mechanism being used.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the AUTHENTICATE
            command, such as authentication failure or an invalid
            authentication mechanism.

        Notes
        -----
        * Client Command - Not Authenticated State;
        * The `mech` parameter specifies the authentication mechanism to use.
          This should be provided as a bytes object;
        * The `authobject` parameter specifies the authentication object or
          objects required by the authentication mechanism;
        * The return value is a dictionary containing information about the
          authentication process, typically including success status and any
          additional data exchanged during the authentication;
        * RFC3501: [AUTHENTICATE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.2.2)
        """

        name = b'AUTHENTICATE'

        try:
            if isinstance(authobject, bytes) or callable(authobject):
                self.push_continuation(authobject)
            elif isinstance(authobject, list):
                for obj in authobject:
                    self.push_continuation(obj)
        except:
            self.push_continuation(authobject)

        try:
            status, tag = self._process_command(name, mech)
            if status == 'OK':
                self.state = 'AUTH'
            else:
                raise self.Error('Could not login.')
        except self.Error:
            raise self.Error('Could not login.')

        return status, tag, None

    def capability(self) -> ResponseType:
        """
        Retrieve a listing of capabilities that the IMAP server supports.

        This method sends the CAPABILITY command to the IMAP server and returns
        the capabilities of the server as a list. These capabilities indicate
        the functionality supported by the IMAP server, such as specific IMAP
        extensions and authentication methods.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - a list of capabilities (List[str]).

        Notes
        -----
        * Client Command - Any State;
        * The CAPABILITY command can be issued at any time during the IMAP
          session and is useful for clients to adapt their behavior based on
          the server's capabilities. It is not necessary to issue this command
          more than once in a session, as the capabilities of the server are
          unlikely to change;
        * Updates `IMAP4P.sstatus["capability"]` with the capability list;
        * RFC3501:
            * [CAPABILITY Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.1.1)
            * [CAPABILITY Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.1)
        """

        name = b'CAPABILITY'

        self.sstatus['capability'] = []

        status, tag = self._process_command(name)
        return status, tag, self.sstatus['capability']

    def check(self) -> ResponseType:
        """
        Requests the server to perform a checkpoint of the currently selected
        mailbox.

        A checkpoint refers to the server doing housekeeping activities such as
        expunging deleted messages, updating the mailbox's status, and
        resolving any pending server-side changes. This method is typically
        used to ensure that updates to the mailbox are synchronized and that
        the mailbox is in a consistent state.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Selected State;
        * The `check` command is part of the standard IMAP protocol and is used
          to request that the server ensure the mailbox is in a consistent
          state. It does not take any arguments and does not modify the state
          of any messages in the mailbox;
        * RFC3501: [CHECK Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.1)
        """

        name = b'CHECK'

        status, tag = self._process_command(name)
        return status, tag, None

    def close(self) -> ResponseType:
        """
        Close the currently selected mailbox.

        After calling this method, no mailbox will be selected, and any changes
        made to the mailbox will be committed (e.g., message deletions).

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Selected State;
        * This method should be called when finished with the currently
          selected mailbox. It is important to close the mailbox before logging
          out or selecting another mailbox to ensure proper cleanup and saving
          of changes;
        * This is the recommended command before "LOGOUT";
        * RFC3501: [CLOSE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.2)
        """

        name = b'CLOSE'

        try:
            status, tag = self._process_command(name)
        finally:
            self.state = 'AUTH'

        return status, tag, None

    def copy(self, message_list: List[int], mailbox: str) -> ResponseType:
        """
        Copy one or more messages from the current selected mailbox to the
        specified destination mailbox.

        This method sends the COPY or UID COPY command to the server to copy
        the messages specified by their sequence numbers or UIDs in the current
        selected mailbox to the specified destination mailbox.

        If the server has the capability 'IMAP4REV1' the UID version will be
        used.

        Parameters
        ----------
        message_list
            A list of integers representing the sequence numbers or the UIDs of
            the messages to be copied. Each integer corresponds to the sequence
            number of a message in the current mailbox.

        mailbox
            The name of the destination mailbox to which the messages will be
            copied.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the COPY or UID COPY
            command, such as permission denied or invalid mailbox.

        Notes
        -----
        * Client Command - Selected State;
        * If the server supports the UIDPLUS a successful COPY ou UID COPY
          command will return a COPYUID message. See [RFC2359 - COPYUID
          response code](https://datatracker.ietf.org/doc/html/rfc2359#section-4.3).
          This response is stored in sstatus['copy_response']
        * RFC3501:
            * [COPY Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.7)
            * [UID Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.8)

        Examples
        --------
        ```pyhton
        >>> client.copy([1, 2, 3], b'Archive')
        ```
        """
        if self.has_uid():
            return self.copy_uid(message_list, mailbox)
        else:
            return self.copy_seq(message_list, mailbox)

    def copy_seq(self, message_list: List[int], mailbox: str) -> ResponseType:
        """
        This is the sequence version of the COPY command. The message set is a list
        of message numbers.

        Please see [IMAP4P.copy][imaplib2.imapp.IMAP4P.copy].
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'COPY'

        message_list_b = shrink_fetch_list(message_list)
        message_list_buf = b','.join(b'%s' % xi for xi in message_list_b)

        args = b'%s "%s"' % (message_list_buf, mailbox_buf)

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['copy_response']

    def copy_uid(self, message_list: List[int], mailbox: str) -> ResponseType:
        """
        This is the UID version of the COPY command. The message set is a list
        of unique identifiers.

        Please see [IMAP4P.copy][imaplib2.imapp.IMAP4P.copy].
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'COPY'

        message_list_b = shrink_fetch_list(message_list)
        message_list_buf = b','.join(b'%s' % xi for xi in message_list_b)

        args = b'%s "%s"' % (message_list_buf, mailbox_buf)

        status, tag = self._process_command_UID(name, args)
        return status, tag, self.sstatus['copy_response']

    def create(self, mailbox: str) -> ResponseType:
        """
        Create a new mailbox on the IMAP server.

        This method sends the CREATE IMAP command to the server to create a new
        mailbox with the specified name.

        Parameters
        ----------
        mailbox
            The name of the new mailbox to be created.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - a dictionary containing a 'MAILBOXID' key if
              the server has the OBJECTID capability, {} otherwise.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the CREATE command, such
            as mailbox already exists or permission denied.

        Notes
        -----
        * Client Command - Authenticated State;
        * The `mailbox` parameter specifies the name of the new mailbox to be
          created;
        * The server will create the specified mailbox if it does not already
          exist;
        * If the mailbox already exists, the server may return an error
          response indicating that the mailbox already exists;
        * RFC3501: [CREATE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.3)
        * RFC8474: [IMAP Extension for Object Identifiers](https://datatracker.ietf.org/doc/html/rfc8474#section-4)

        Examples
        --------
        ```python
        >>> client.create('NewMailbox')
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'CREATE'
        self.sstatus['create_response'] = {}
        args = b'"%s"' % mailbox_buf

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['create_response']

    def delete(self, mailbox: str) -> ResponseType:
        """
        Delete a mailbox from the IMAP server.

        This method sends the DELETE IMAP command to the server to delete the
        specified mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox to be deleted.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the DELETE command, such
            as mailbox not found or permission denied.

        Notes
        -----
        * Client Command - Authenticated State;
        * The `mailbox` parameter specifies the name of the mailbox to be
          deleted;
        * The server will delete the specified mailbox if it exists;
        * If the mailbox does not exist or cannot be deleted due to permission
          restrictions, the server may return an error response;
        * RFC3501: [DELETE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.4).

        Examples
        --------
        ```python
        >>> response = client.delete('OldMailbox')
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'DELETE'
        args = b'"%s"' % mailbox_buf

        status, tag = self._process_command(name, args)
        return status, tag, None

    def deleteacl(self, mailbox: str, identifier: str) -> ResponseType:
        """
        Remove an access control list (ACL) entry for the specified identifier
        from the mailbox.

        This method sends the DELETEACL IMAP command to the server to remove
        any (identifier,rights) pair from the access control list (ACL) for the
        specified mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox from which to remove the ACL entry.

        identifier
            The identifier (e.g., username) for which the ACL entry will be
            removed.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the DELETEACL command,
            such as mailbox not found, identifier not found, or permission denied.

        Notes
        -----
        * The `mailbox` parameter specifies the name of the mailbox from which
          to remove the ACL entry;
        * The `identifier` parameter specifies the identifier (e.g., username)
          for which the ACL entry will be removed;
        * The server must support the ACL capability (RFC4314) for this command
          to be successful;
        * The server may return an error response if the ACL entry does not
          exist or cannot be removed due to permission restrictions;
        * RFC4314: [IMAP ACL Extension](https://datatracker.ietf.org/doc/html/rfc4314#section-3.2).

        Examples
        --------
        ```python
        >>> client.deleteacl('Inbox', 'user@example.com')
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)
        identifier_buf = identifier.encode(self.encoding)

        if not self.has_capability('ACL'):
            raise self.Error('The server does not support the ACL capability.')

        name = b'DELETEACL'
        args = b'"%s" %s' % (mailbox_buf, identifier_buf)

        status, tag = self._process_command(name, args)
        return status, tag, None

    def examine(self, mailbox: str) -> ResponseType:
        """
        Same as the [select][imaplib2.imapp.IMAP4P.select] method but "opens"
        the mailbox read-only.

        The state of the server will be "SELECTED" after a successful EXAMINE
        command.
        """
        return self.select(mailbox, True)

    def expunge(self) -> ResponseType:
        """
        Permanently remove deleted items from the selected mailbox.

        This method sends the EXPUNGE IMAP command to the server to permanently
        remove any messages marked for deletion (e.g., with the '\\Deleted'
        flag) from the currently selected mailbox.

        The expunge responseis ignored.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the EXPUNGE command,
            such as permission denied or mailbox not found.

        Notes
        -----
        * Client Command - Selected State;
        * RFC3501:
            * [EXPUNGE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.3);
            * [EXPUNGE Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.4.1).
        """

        name = b'EXPUNGE'

        status, tag = self._process_command(name)
        return status, tag, None

    def fetch(self, message_list: List[int], message_parts: str) -> ResponseType:
        """
        Fetches specified parts of messages from the server.

        This method sends the FETCH command to the server to retrieve specific
        parts of the messages specified in the message list.

        Parameters
        ----------
        message_list
            A list of message sequence numbers to fetch.
        message_parts
            A string indicating which parts of the messages to fetch. Possible
            values include:

            * '(FLAGS)': Fetches message flags;
            * 'BODY': Fetches a form of BODYSTRUCTURE without extension data;
            * 'BODY[]': Fetches entire message body;
            * 'BODY[HEADER]': Fetches only the message header;
            * 'BODY[TEXT]': Fetches only the message body text;
            * 'BODY[1.2]': Fetches the specified body part;
            * 'BODY.PEEK[]': Fetches the entire message body without setting
              the '\\Seen' flag;
            * 'BODY.PEEK[HEADER]': Fetches only the message header without
              setting the '\\Seen' flag;
            * 'BODY.PEEK[TEXT]': Fetches only the message body text without
              setting the '\\Seen' flag;
            * 'BODY.PEEK[1.2]': Fetches the specified body part without
              setting the '\\Seen' flag;
            * 'ENVELOPE': Fetches the message envelope, which contains
              essential message metadata such as sender, recipient, subject,
              and date.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - (Dict[int, FetchParser]) A dictionary
              containing fetched message data.

        Notes
        -----
        * Client Command - Selected State;
        * The [FetchParser][imaplib2.parsefetch.FetchParser] class parses the
          fetch response and creates python data structures suitable for each
          kid of response;
        * RFC3501:
            * [FETCH Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.5).
            * [FETCH Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.4.2).
        """

        if self.has_uid():
            return self.fetch_uid(message_list, message_parts)
        else:
            return self.fetch_seq(message_list, message_parts)

    def fetch_seq(self, message_list: List[int], message_parts: str = '(FLAGS)') -> ResponseType:
        """
        This is the sequence version of the FETCH command. The message set is a list
        of message numbers.

        Please see [IMAP4P.fetch][imaplib2.imapp.IMAP4P.fetch].
        """
        return self._fetch(False, message_list, message_parts)

    def fetch_uid(self, message_list: List[int], message_parts: str = '(FLAGS)') -> ResponseType:
        """
        This is the UID version of the FETCH command. The message set is a list
        of unique identifiers.

        Please see [IMAP4P.fetch][imaplib2.imapp.IMAP4P.fetch].
        """
        return self._fetch(True, message_list, message_parts)

    def getacl(self, mailbox: str) -> ResponseType:
        """
        This method sends the GETACL IMAP command to the server to retrieve the
        access control lists (ACLs) associated with the specified mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox for which to retrieve the access control
            lists (ACLs).

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A dictionary containing information about the
              access control lists (ACLs) for the specified mailbox. The
              dictionary maps each identifier (e.g., username) to the
              corresponding set of rights granted. The dictionary has this structure:
              ```python
              {
              'mailbox': str,
              'acl': {
              'identifier': 'acl str',
              ...
              }
              }
              ```

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the GETACL command,
            such as mailbox not found or permission denied.

        Notes
        -----
        * The GETACL command retrieves the access control lists (ACLs)
          associated with the specified mailbox;
        * The server must support the ACL capability (RFC4314) for this command
          to be successful;
        * The returned dictionary contains information about the access control
          lists (ACLs) for the specified mailbox, where each key is an
          identifier (e.g., username) and the corresponding value is a set of
          rights granted to that identifier;
        * RFC4314: [IMAP ACL Extension](https://datatracker.ietf.org/doc/html/rfc4314#section-3.3).

        Examples
        --------
        ```python
        >>> acl_info = client.getacl('Inbox')
        >>> print(acl_info)
        {'user1@example.com': 'lrswipcda', 'user2@example.com': 'lr'}
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        if not self.has_capability('ACL'):
            raise self.Error('The server does not support the ACL capability.')

        name = b'GETACL'
        args = b'"%s"' % mailbox_buf

        self.sstatus['acl_response'] = {'mailbox': mailbox, 'acl': {}}

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['acl_response']

    def list(
        self,
        directory: str = '',
        pattern: str = '*',
        selection_opts: Optional[list[str]] = None,
        options: Optional[list[str]] = None,
    ) -> ResponseType:
        """
        This method sends the LIST IMAP command to the server to list mailboxes
        matching the specified base directory and pattern.

        Parameters
        ----------
        directory
            The directory to search within. Defaults to an empty byte string,
            indicating the root directory.

        pattern
            The pattern used to match mailbox names.
            Default is '*', which matches all mailbox names.
            Common patterns include '*' (matches all), '%' (matches any one
            level), and 'user.*' (matches all mailboxes starting with 'user.').

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - (list[imaplib2.parselist.Mailbox]) A list of
              Mailbox objects representing the mailboxes matching the specified
              directory and pattern.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the LIST command, such
            as authentication failure or an invalid command.

        Notes
        -----
        * Client Command - Authenticated State;
        * The `directory` parameter specifies the directory within which to
          search for mailboxes. If not provided or empty, the root directory is
          assumed.
        * The `pattern` parameter follows the IMAP wildcard pattern syntax. The
          parameter specifies the pattern to match mailbox names against. The
          default pattern, b'*', matches all mailbox names.
        * Common wildcard characters include '*', which matches zero or more
          characters, and '%', which matches any one level of hierarchy.
        * The returned list contains Mailbox objects representing the mailboxes
          found on the server that match the specified directory and pattern.
        * Firefox uses the following command to retrieve the mailbox list:
            * `list (subscribed) "" "*" return (special-use)`
        * RFC3501:
            * [LIST Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.8)
            * [LIST Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.2)
        * RFC5258: [LIST Command Extensions](https://datatracker.ietf.org/doc/html/rfc5258)

        Examples
        --------
        ```python
        >>> mailboxes = client.list()
        >>> for mailbox in mailboxes:
        ...     print(mailbox)
        ```
        """

        # Convert arguments to bytes
        directory_buf = encode_to_utf7(directory)
        pattern_buf = encode_to_utf7(pattern)

        name = b'LIST'
        args = b'"%s" "%s"' % (directory_buf, pattern_buf)

        self.sstatus['list_response'] = []

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['list_response']

    def listrights(self, mailbox: str, identifier: str) -> ResponseType:
        """
        Get information about the rights that can be granted to an identifier
        in the ACL for a specific mailbox.

        This method sends the LISTRIGHTS IMAP command to the server to retrieve
        information about the rights that can be granted to the specified
        identifier in the access control list (ACL) for the specified mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox for which to retrieve the rights
            information.

        identifier
            The identifier (e.g., username) for which to retrieve the rights
            information.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A dictionary containing information about the
              [rights](https://datatracker.ietf.org/doc/html/rfc4314#section-2.1)
              that can be granted to the specified identifier in the
              access control list (ACL) for the specified mailbox. The
              dictionary has the following keys:
                * mailbox
                * identifier
                * req_rights - set of rights the identifier will always be
                  granted in the mailbox
                * opt_rights - zero or more strings each containing a set of
                  rights the identifier can be granted in the mailbox

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the LISTRIGHTS command,
            such as mailbox not found or permission denied.

        Notes
        -----
        * The LISTRIGHTS command retrieves information about the rights that
          can be granted to the specified identifier in the access control list
          (ACL) for the specified mailbox;
        * The server must support the ACL capability (RFC4314) for this command
          to be successful;
        * RFC4314 - IMAP ACL Extension:
            * [LISTRIGHTS Command](
            https://datatracker.ietf.org/doc/html/rfc4314#section-3.4);
            * [LISTRIGHTS Response](
            https://datatracker.ietf.org/doc/html/rfc4314#section-3.7).
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)
        identifier_buf = identifier.encode(self.encoding)

        if not self.has_capability('ACL'):
            raise self.Error('The server does not support the ACL capability.')

        name = b'LISTRIGHTS'
        args = b'"%s" %s' % (mailbox_buf, identifier_buf)

        self.sstatus['listrights_response'] = {
            'mailbox': '',
            'identifier': '',
            'req_rights': '',
            'opt_rights': (),
        }

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['listrights_response']

    def login(self, user: str, password: str) -> ResponseType:
        """
        Log in to the IMAP server with the given username and password.

        This method sends the LOGIN command to the IMAP server and attempts to
        authenticate the user with the provided credentials.

        Parameters
        ----------
        user
            The username to log in with.
        password
            The password corresponding to the user.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.error
            If the login attempt fails due to incorrect credentials or other
            IMAP errors.

        Notes
        -----
        * Client Command - Not Authenticated State;
        * The `password` will be quoted;
        * RFC3501: [LOGIN Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.2.3)
        """
        # Convert arguments to bytes
        user_buf = user.encode(self.encoding)
        password_buf = password.encode(self.encoding)

        # Send the command
        name = b'LOGIN'
        args = b'%s "%s"' % (user_buf, password_buf)

        try:
            status, tag = self._process_command(name, args)
            if status == 'OK':
                self.state = 'AUTH'
            else:
                raise self.Error('Could not login.')
        except self.Error:
            raise self.Error('Could not login.')

        return status, tag, None

    def login_cram_md5(self, user: str, password: str) -> ResponseType:
        """
        Log in to the IMAP server using CRAM-MD5 authentication.

        This method attempts to log in to the IMAP server using the CRAM-MD5
        authentication mechanism. It first checks if the server supports the
        AUTH=DIGEST-MD5 capability. If the server supports it, the method
        sets the username and password attributes, and then calls the
        authenticate method with 'CRAM-MD5' as the mechanism and the
        _CRAM_MD5_AUTH function as the authobject.

        Parameters
        ----------
        user
            The username to be used for authentication.

        password
            The password associated with the provided username.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * This method calls [authenticate][imaplib2.imapp.IMAP4P.authenticate].

        Raises
        ------
        IMAP4P.Error
            If the server does not support the AUTH=DIGEST-MD5 capability.
        """
        # Convert arguments to bytes
        user_buf = user.encode(self.encoding)
        password_buf = password.encode(self.encoding)

        if not self.has_capability('AUTH=DIGEST-MD5'):
            raise self.Error('The server does not support the AUTH=DIGEST-MD5 capability.')
        self.user, self.password = user_buf, password_buf
        return self.authenticate(b'CRAM-MD5', self._CRAM_MD5_AUTH)

    def login_plain(self, user: str, password: str) -> ResponseType:
        """
        Log in to the IMAP server using the PLAIN authentication mechanism.

        This method attempts to log in to the IMAP server using the PLAIN
        authentication mechanism. It first checks if the server supports the
        AUTH=PLAIN capability. If the server supports it, the method constructs
        the authentication credentials using the provided username and
        password, encodes them in base64 format, and sends them to the server
        using the AUTHENTICATE command.

        Parameters
        ----------
        user
            The username to be used for authentication.

        password
            The password associated with the provided username.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If the server does not support the AUTH=PLAIN capability.

        Notes
        -----
        * The PLAIN authentication mechanism requires the server to support the
          AUTH=PLAIN capability;
        * The username and password are encoded in base64 format as per the
          PLAIN authentication mechanism;
        * The server must verify the authentication credentials and respond
          accordingly;
        * This method calls [authenticate][imaplib2.imapp.IMAP4P.authenticate].
        """
        # Convert arguments to bytes
        user_buf = user.encode(self.encoding)
        password_buf = password.encode(self.encoding)

        if not self.has_capability('AUTH=PLAIN'):
            raise self.Error('The server does not support the AUTH=PLAIN capability.')

        import base64

        auth_tokens = base64.b64encode(b'%s\0%s\0%s' % (user_buf, user_buf, password_buf))

        return self.authenticate(b'PLAIN', auth_tokens)

    def login_login(self, user: str, password: str) -> ResponseType:
        """
        Log in to the IMAP server using the LOGIN authentication mechanism.

        This method attempts to log in to the IMAP server using the LOGIN
        authentication mechanism. It first checks if the server supports the
        AUTH=LOGIN capability. If the server supports it, the method encodes
        the username and password in base64 format, constructs the
        authentication tokens, and sends them to the server using the
        AUTHENTICATE command with the 'LOGIN' mechanism.

        Parameters
        ----------
        user
            The username to be used for authentication.

        password
            The password associated with the provided username.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If the server does not support the AUTH=LOGIN capability.
        """
        # Convert arguments to bytes
        user_buf = user.encode(self.encoding)
        password_buf = password.encode(self.encoding)

        if not self.has_capability('AUTH=LOGIN'):
            raise self.Error('The server does not support the AUTH=LOGIN capability.')

        import base64

        auth_tokens = [base64.b64encode(user_buf), base64.b64encode(password_buf)]

        return self.authenticate(b'LOGIN', auth_tokens)

    def logout(self) -> ResponseType:
        """
        Log out from the IMAP server.

        This method sends the LOGOUT command to the server to terminate the IMAP session and
        gracefully log out from the server. It updates the internal state of the IMAP client
        to indicate that it is in the LOGOUT state.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Any State;
        * RFC3501: [LOGOUT Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.1.3)
        """
        name = b'LOGOUT'
        self.state = 'LOGOUT'
        try:
            status, tag = self._process_command(name)
            return status, tag, None
        except ssl.SSLError:
            return 'NO', '', self.sstatus

    def lsub(self, directory: str = '', pattern: str = '*') -> ResponseType:
        """
        List subscribed mailbox names in a directory matching a specified
        pattern.

        This method sends the LSUB command to the server to list the mailbox
        names that are subscribed by the current user within the specified
        directory and matching the specified pattern.

        Parameters
        ----------
        directory
            The directory within which to search for subscribed mailbox names.
            Default is an empty string, indicating the root directory.

        pattern
            The pattern used to match mailbox names.
            Default is '*', which matches all mailbox names.
            Common patterns include '*' (matches all), '%' (matches any one
            level), and 'user.*' (matches all mailboxes starting with 'user.').

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - (list[imaplib2.parselist.Mailbox]) A list of
              Mailbox objects representing the mailboxes matching the specified
              directory and pattern.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the LIST command, such
            as authentication failure or an invalid command.

        Notes
        -----
        * Client Command - Authenticated State;
        * The `directory` parameter specifies the directory within which to
          search for mailboxes. If not provided or empty, the root directory is
          assumed.
        * The `pattern` parameter follows the IMAP wildcard pattern syntax. The
          parameter specifies the pattern to match mailbox names against. The
          default pattern, b'*', matches all mailbox names.
        * Common wildcard characters include '*', which matches zero or more
          characters, and '%', which matches any one level of hierarchy.
        * The returned list contains Mailbox objects representing the mailboxes
          found on the server that match the specified directory and pattern.
        * RFC3501:
            * [LSUB Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.9)
            * [LSUB Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.3)
        """
        # Convert arguments to bytes
        directory_buf = encode_to_utf7(directory)
        pattern_buf = encode_to_utf7(pattern)

        name = b'LSUB'
        args = b'"%s" "%s"' % (directory_buf, pattern_buf)

        self.sstatus['list_response'] = []

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['list_response']

    def myrights(self, mailbox: str) -> ResponseType:
        """
        Get the rights that the user has to a specific mailbox.

        This method sends the MYRIGHTS command to the server to retrieve the
        set of rights that the current user has to the specified mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox for which to retrieve the user's rights.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A dictionary containing information about the
              user's rights to the specified mailbox. The dictionary has two
              keys:
                * 'mailbox': The name of the mailbox;
                * 'rights': A string representing the rights that the user has
                  to the mailbox.

        Raises
        ------
        IMAP4P.Error
            If the server does not support the ACL capability (RFC4314).

        Notes
        -----
        * The MYRIGHTS command returns the set of rights that the user has to
          the specified mailbox;
        * The server must support the ACL capability (RFC4314) for this command
          to be successful;
        * The rights are represented as a string, where each character denotes
          a specific right (e.g., 'lrswipcda'). See RFC4314, [Standard
          Rights](https://datatracker.ietf.org/doc/html/rfc4314#section-2.1)
          for details on the meaning of each character.
        * RFC4314 - IMAP ACL Extension:
            * [MYRIGHTS Command](https://datatracker.ietf.org/doc/html/rfc4314#section-3.5);
            * [MYRIGHTS Response](https://datatracker.ietf.org/doc/html/rfc4314#section-3.8).
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        if not self.has_capability('ACL'):
            raise self.Error('The server does not support the ACL capability.')

        name = b'MYRIGHTS'
        args = b'"%s"' % (mailbox_buf)

        self.sstatus['myrights_response'] = {'mailbox': '', 'rights': ''}

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['myrights_response']

    def namespace(self) -> ResponseType:
        """
        Retrieve the namespace prefixes from the server.

        This method sends the NAMESPACE command to the server to retrieve the
        namespace prefixes. Namespace prefixes define the hierarchical
        structure of mailboxes on the server.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A dict containing the name spaces personal for
              the user, for the other users ans the shared name spaces. Each
              name space is composed by a prefix and a separator.

        Raises
        ------
        IMAP4P.Error
            If the server does not support the NAMESPACE capability (RFC2342)
            or IMAP Version 4rev2 (RFC9051).

        Notes
        -----
        * The NAMESPACE command retrieves the namespace prefixes from the
          server;
        * The retrieved namespace prefixes provide information about the
          hierarchical structure of mailboxes on the server;
        * RFC2342: [IMAP4 Namespace](https://www.rfc-editor.org/rfc/rfc2342.html).

        Examples
        --------
        ```python
        >>> namespace_info = client.namespace()
        >>> print(namespace_info)
        [["" "/"]] [["Other Users/" "/"]] [["Shared Folders/" "/"]]
        ```
        """
        if not (self.has_capability('NAMESPACE') or self.has_capability('IMAP4rev2')):
            raise self.Error('The server does not support the NAMESPACE or IMAP4rev2 capability.')

        name = b'NAMESPACE'
        self.sstatus['namespace'] = {
            'personal': [],
            'other_users': [],
            'shared': [],
        }
        status, tag = self._process_command(name)
        return status, tag, self.sstatus['namespace']

    def noop(self) -> ResponseType:
        """
        Send a NOOP command to the server.

        This method sends a NOOP (no-operation) command to the IMAP server. The
        NOOP command is used to request a server acknowledgment to confirm that
        the connection and session are still alive, but it does not perform any
        actual operation on the server.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Any State;
        * The NOOP command is typically used to keep the connection alive
          during idle periods;
        * The server response to the NOOP command can provide information about
          the server status;
        * RFC3501: [NOOP Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.1.2).
        """
        name = b'NOOP'

        status, tag = self._process_command(name)
        return status, tag, None

    def rename(self, old_mailbox: str, new_mailbox: str) -> ResponseType:
        """
        Rename a mailbox on the server.

        This method renames a mailbox on the server from `old_mailbox` to
        `new_mailbox`.

        Parameters
        ----------
        old_mailbox
            The name of the existing mailbox to be renamed.

        new_mailbox
            The new name for the mailbox.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        Error
            If an error occurs during the renaming process, such as the
            specified mailbox not existing or the operation failing.

        Notes
        -----
        - Renaming a mailbox may also move its contents, depending on the
        server implementation.
        - The `oldmailbox` parameter must be a valid mailbox name that exists
        on the server.
        - The `newmailbox` parameter must be a valid mailbox name that does not
        currently exist on the server.
        - RFC3501: [RENAME Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.5)

        Examples
        --------
        ```python
        >>> client.rename(b'INBOX.Test', b'INBOX.NewTest')
        ```
        """
        # Convert arguments to bytes
        old_mailbox_buf = encode_to_utf7(old_mailbox)
        new_mailbox_buf = encode_to_utf7(new_mailbox)

        name = b'RENAME'
        args = b'"%s" "%s"' % (old_mailbox_buf, new_mailbox_buf)

        status, tag = self._process_command(name, args)
        return status, tag, None

    def search(self, criteria: str, charset: Optional[str] = None) -> ResponseType:
        """
        Search for messages in the currently selected mailbox that match the
        given criteria.

        Parameters
        ----------

        criteria
            A string of one or more search keys, as defined by the IMAP SEARCH
            command. These keys can be used to search various message attributes
            such as date, flags, sender, etc. The criteria should be specified in
            IMAP format, which is a space-separated list of conditions.

            Examples of valid criteria:

            * 'ALL': retrieves all messages.
            * 'UNSEEN': retrieves messages that have not been marked as seen.
            * 'FROM "example@example.com"': retrieves messages from a specific
              sender.
            * 'SINCE "01-Jan-2020"': retrieves messages since a certain date.
            * 'SUBJECT "urgent" NOT SEEN': retrieves messages with 'urgent' in
              the subject that are not seen.
            * 'OR TO boss SUBJECT resignation': retrieves messages that either
              have 'boss' in the 'To' field or contain 'resignation' in the
              subject line.

        charset :
            The character set to use when searching strings. If not specified,
            defaults to UTF-8. This is useful when searching for strings that
            contain non-ASCII characters.

            Example:

            *  'utf-8': Use UTF-8 charset for the search.
            *  'iso-8859-1': Use ISO-8859-1 charset for the search.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - (list[int]) If the server has the UID
              capability this command will return a list of unique identifiers.
              If not it returns a list of message sequence numbers.

        Raises
        ------
        IMAP4P.Error
            If the criteria string is not formatted correctly according to IMAP
            standards.

        Notes
        -----

        * RFC3501:
            * [SEARCH Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.4)
            * [SEARCH Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.5)
            * [UID Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.8)
        """
        if self.has_uid():
            return self.search_uid(criteria, charset)
        else:
            return self.search_seq(criteria, charset)

    def search_seq(self, criteria: str, charset: Optional[str] = None) -> ResponseType:
        """
        This is the sequence version of the SEARCH command. It returns message
        sequence numbers.

        Please see [IMAP4P.search][imaplib2.imapp.IMAP4P.search].
        """
        # Convert arguments to bytes
        criteria_buf = criteria.encode(self.encoding)
        if charset:
            charset_buf = charset.encode(self.encoding)

        name = b'SEARCH'
        self.sstatus['search_response'] = ()
        if charset:
            args = b'CHARSET %s %s' % (charset_buf, criteria_buf)
        else:
            args = criteria_buf

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['search_response']

    def search_uid(self, criteria: str, charset: Optional[str] = None) -> ResponseType:
        """
        This is the UID version of the SEARCH command. It returns unique identifiers.

        Please see [IMAP4P.search][imaplib2.imapp.IMAP4P.search].
        """
        # Convert arguments to bytes
        criteria_buf = criteria.encode(self.encoding)
        if charset:
            charset_buf = charset.encode(self.encoding)

        name = b'SEARCH'
        self.sstatus['search_response'] = ()
        if charset:
            args = b'CHARSET %s %s' % (charset_buf, criteria_buf)
        else:
            args = criteria_buf
        status, tag = self._process_command_UID(name, args)
        return status, tag, self.sstatus['search_response']

    def select(self, mailbox: str, readonly: bool = False) -> ResponseType:
        """
        Select a mailbox on the server for subsequent operations.

        This method sends the SELECT or the EXAMINE IMAP command to the server
        to select the specified mailbox for further operations.

        Parameters
        ----------
        mailbox
            The name of the mailbox to be selected.

        readonly
            Whether to open the mailbox in read-only mode. Defaults to False,
            indicating read-write mode.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - a dictionary with the following keys:
                * Name
                * Expunge list
                * Is readonly
                * PERMANENTFLAGS
                * UNSEEN
                * UIDVALIDITY
                * UIDNEXT
                * HIGHESTMODSEQ
                * ANNOTATIONS
                * MAILBOXID
                * URLMECH

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the SELECT or EXAMINE
            command, such as the specified mailbox not existing or permission
            denied.

        Notes
        -----
        * Client Command - Authenticated State;
        * The `folder` parameter specifies the name of the mailbox to be
          selected;
        * The `readonly` parameter determines whether the mailbox is opened in
          read-only mode. If True, no modifications are allowed to the mailbox;
        * RFC3501:
            - [SELECT Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.1)
            - [EXAMINE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.2)
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        if readonly:
            name = b'EXAMINE'
        else:
            name = b'SELECT'
        args = b'"%s"' % mailbox_buf

        self.sstatus['current_folder'] = {}
        self.sstatus['current_folder']['name'] = mailbox

        status, tag = self._process_command(name, args)

        if status == 'OK':
            self.state = 'SELECTED'
        else:
            self.state = 'AUTH'

        return status, tag, self.sstatus['current_folder']

    def setacl(self, mailbox: str, identifier: str, acl: str) -> ResponseType:
        """
        Changes the access control list (ACL) on the specified mailbox.

        The SETACL command changes the access control list on the specified
        mailbox so that the specified identifier is granted permissions as
        specified in the third argument.

        Parameters
        ----------
        mailbox
            The name of the mailbox whose ACL is to be modified.
        identifier
            The identifier (user or group) to which permissions will be
            granted.
        acl
            The access control list specifying the permissions to be granted to
            the identifier. The format of the ACL string should follow the
            syntax specified in RFC 4314.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Raises
        ------
        IMAP4P.Error
            If an error occurs during the execution of the SETACL command,
            such as mailbox not found, identifier not found, or permission denied.

        Notes
        -----
        * The `mailbox` parameter specifies the name of the mailbox;
        * The `identifier` parameter specifies the identifier (e.g., username)
          for which the ACL entry will be set;
        * The server must support the ACL capability (RFC4314) for this command
          to be successful;
        * The server may return an error response if the ACL entry does not
          exist or cannot be set due to permission restrictions;
        * RFC4314: [IMAP ACL Extension](https://datatracker.ietf.org/doc/html/rfc4314#section-3.2).
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)
        identifier_buf = identifier.encode(self.encoding)
        acl_buf = acl.encode(self.encoding)

        if not self.has_capability('ACL'):
            raise self.Error('The server does not support the ACL capability.')

        name = b'SETACL'
        args = b'"%s" %s %s' % (mailbox_buf, identifier_buf, acl_buf)

        status, tag = self._process_command(name, args)
        return status, tag, None

    def sort(self, program: str, charset: str, search_criteria: str) -> ResponseType:
        """
        Fetches and sorts messages from the server based on the specified
        criteria.

        This command attempts to use the SORT extension to retrieve messages
        using UIDs. If the SORT extension is not available on the server, it
        will degrade to either the non-UID SORT command or the SEARCH command,
        depending on the server capabilities. Note that no attempt is made to
        emulate the behavior of more complex commands on the degraded ones.

        Parameters
        ----------
        program
            The sorting program to be used. This specifies the sort criteria
            and can include various sorting parameters such as date, arrival
            order, subject, etc.

            Sort programs:

            * ARRIVAL: Internal date and time of the message;
            * CC: addr-mailbox of the first "cc" address;
            * DATE: Sent date and time;
            * FROM: addr-mailbox of the first "From" address;
            * SIZE: Size of the message in octets;
            * SUBJECT: Base subject text;
            * TO: addr-mailbox of the first "To" address;
            * REVERSE: Followed by another sort criterion, has the effect of
              that criterion but in reverse (descending) order.

        charset
            The character set to be used for sorting. This specifies the
            character encoding of the messages.

            Example:

            *  'utf-8': Use UTF-8 charset for the search;
            *  'us-ascii': Use US-ASCII charset for the search.

        search_criteria
            The search criteria to filter the messages before sorting. This can
            be any valid IMAP search criteria.

            See the [search criteria][imaplib2.imapp.IMAP4P.search] in the
            search method for examples of search criteria.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A tuple containing the sorted message sequence
              numbers or corresponding UIDs (if available) based on the
              specified criteria.

        Notes
        -----

        * RFC5256: [SORT Command](https://datatracker.ietf.org/doc/html/rfc5256);
        * RFC3501:
            * [SEARCH Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.4)
            * [SEARCH Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.5)
            * [UID Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.8)

        Examples
        --------
        To sort messages by date in ascending order and retrieve the corresponding UIDs:
        ```python
        >>> sort_criteria = 'DATE (REVERSE)'
        >>> charset = 'UTF-8'
        >>> search_criteria = 'ALL'
        >>> sorted_messages = imap.sort(sort_criteria, charset, search_criteria)
        ```

        To sort messages by subject in descending order and retrieve only unseen messages:
        ```python
        >>> sort_criteria = 'SUBJECT (REVERSE)'
        >>> charset = 'UTF-8'
        >>> search_criteria = 'UNSEEN'
        >>> sorted_messages = imap.sort(sort_criteria, charset, search_criteria)
        ```
        """
        if self.has_uid():
            if self.has_capability('SORT'):
                return self.sort_uid(program, charset, search_criteria)
            else:
                return self.search_uid(search_criteria, charset)
        elif self.has_capability('SORT'):
            return self.sort_seq(program, charset, search_criteria)
        else:
            return self.search_seq(search_criteria, charset)

    def sort_seq(self, program: str, charset: str, search_criteria: str) -> ResponseType:
        """
        This is the sequence version of the SORT command. It returns unique
        sequence numbers.

        The server must support the SORT extension.

        Please see [IMAP4P.sort][imaplib2.imapp.IMAP4P.sort].
        """
        # Convert arguments to bytes
        program_buf = program.encode(self.encoding)
        charset_buf = charset.encode(self.encoding)
        search_criteria_buf = search_criteria.encode(self.encoding)

        if not self.has_capability('SORT'):
            raise self.Error('The server does not support the SORT capability.')

        name = b'SORT'

        self.sstatus['sort_response'] = ()

        args = b'%s %s %s' % (program_buf, charset_buf, search_criteria_buf)
        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['sort_response']

    def sort_uid(self, program: str, charset: str, search_criteria: str) -> ResponseType:
        """
        This is the UID version of the SORT command. It returns message
        unique identifiers.

        The server must support the UIDPLUS extension and the SORT extension.

        Please see [IMAP4P.sort][imaplib2.imapp.IMAP4P.sort].
        """
        # Convert arguments to bytes
        program_buf = program.encode(self.encoding)
        charset_buf = charset.encode(self.encoding)
        search_criteria_buf = search_criteria.encode(self.encoding)

        if not self.has_capability('SORT'):
            raise self.Error('The server does not support the SORT capability.')

        name = b'SORT'
        self.sstatus['sort_response'] = ()
        args = b'%s %s %s' % (program_buf, charset_buf, search_criteria_buf)
        status, tag = self._process_command_UID(name, args)
        return status, tag, self.sstatus['sort_response']

    def status(self, mailbox: str, names: List[str]) -> ResponseType:
        """
        Get the status of a mailbox.

        This method sends the STATUS command to the server to retrieve the
        status information for the specified mailbox. The status information
        includes various attributes such as the number of messages, the UID
        validity, and the recent message count.

        Parameters
        ----------
        mailbox
            The name of the mailbox for which to retrieve the status.

        names
            A list of strings containing the names of the status attributes to be
            retrieved. This can include one or more of the following
            attributes: 'MESSAGES', 'RECENT', 'UIDNEXT', 'UIDVALIDITY', and
            'UNSEEN'.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - A dictionary containing the status information
              for the specified mailbox. The keys of the dictionary correspond
              to the requested status attributes, and the values represent the
              corresponding values retrieved from the server.

        Notes
        -----
        * Client Command - Authenticated State;
        * The STATUS command retrieves status information about a specific mailbox from the server;
        * The 'names' parameter specifies which attributes to retrieve. The
          supported attributes are:
            * 'MESSAGES': The number of messages in the mailbox;
            * 'RECENT': The number of messages with the \\Recent flag set;
            * 'UIDNEXT': The next UID that will be assigned to a new message in the mailbox;
            * 'UIDVALIDITY': The unique identifier validity value for the mailbox;
            * 'UNSEEN': The number of unseen messages in the mailbox;
        * The server response contains the requested status information;
        * RFC3501:
            * [STATUS Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.10);
            * [STATUS Response](https://datatracker.ietf.org/doc/html/rfc3501#section-7.2.4).

        Examples
        --------
        ```python
        >>> status_info = client.status('INBOX', 'MESSAGES UNSEEN')
        ```
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)
        names_buf = list_to_bytes(names, self.encoding)

        name = b'STATUS'
        args = b'"%s" (%s)' % (mailbox_buf, b' '.join(names_buf))

        self.sstatus['status_response'] = {}

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['status_response']

    def store(
        self, message_set: Union[int, List[int]], command: str, flags: List[str]
    ) -> ResponseType:
        """
        Modify message flags or metadata for the specified message set.

        This method checks if the UIDPLUS extension is available on the server.
        If the UIDPLUS extension is available, it uses the "UID STORE" command
        to modify message flags or metadata for the specified message set.
        Otherwise, it uses the plain "STORE" command.

        Parameters
        ----------
        message_set
            A list of message sequence numbers or UIDs indicating the messages
            to be modified. Each element in the list should be a string
            representing a single message sequence number or UID.

        command
            The type of modification to perform. Possible values include:
            * '+FLAGS': Add the specified flags to the messages.
            * '-FLAGS': Remove the specified flags from the messages.
            * 'FLAGS': Replace the current flags of the messages with the
              specified flags.

        flags
            A list of flags to be modified. Each flag should be a string.
            Possible flags include system flags (e.g., '\\Seen', '\\Flagged',
            '\\Deleted') and custom flags defined by the server.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

            Note that the STORE command will return untagged FETCH responses,
            these are stored on IMAP4P.sstatus['fetch_response'].

        Notes
        -----
        * Client Command - Selected State;
        * The STORE command is used to modify message flags or metadata for the
          specified messages;
        * The message_set parameter can contain either message sequence numbers
          or UIDs;
        * The server response contains information about the result of the
          STORE command:
            * A FETCH FLAGS response;
            * A FLAGS response with the flags of the current selected mailbox;
        * RFC3501: [STORE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.6).

        Examples
        --------
        ```python
        >>> result = client.store([1, 2, 3], '+FLAGS', ['\\Seen'])
        ```
        """
        if self.has_uid():
            return self.store_uid(message_set, command, flags)
        else:
            return self.store_seq(message_set, command, flags)

    def store_seq(
        self, message_set: Union[int, List[int]], command: str, flags: List[str]
    ) -> ResponseType:
        """
        This is the sequence version of the STORE command. It returns unique
        sequence numbers.

        Please see [IMAP4P.store][imaplib2.imapp.IMAP4P.store].
        """
        # Convert arguments to bytes
        command_buf = command.encode(self.encoding)
        flags_buf = b'(' + b' '.join([flag.encode(self.encoding) for flag in flags]) + b')'

        name = b'STORE'

        if isinstance(message_set, list):
            message_set_buf = [b'%d' % msg_id for msg_id in message_set]
            message_buf = b','.join(message_set_buf)
        elif isinstance(message_set, int):
            message_buf = b'%d' % message_set

        args = b'%s %s %s' % (message_buf, command_buf, flags_buf)
        status, tag = self._process_command(name, args)
        return status, tag, None

    def store_uid(
        self, message_set: Union[int, List[int]], command: str, flags: List[str]
    ) -> ResponseType:
        """
        This is the UID version of the STORE command. It returns message
        unique identifiers.

        Please see [IMAP4P.store][imaplib2.imapp.IMAP4P.store].
        """
        # Convert arguments to bytes
        command_buf = command.encode(self.encoding)
        flags_buf = b'(' + b' '.join([flag.encode(self.encoding) for flag in flags]) + b')'

        name = b'STORE'

        if isinstance(message_set, list):
            message_set_buf = [b'%d' % msg_id for msg_id in message_set]
            message_buf = b','.join(message_set_buf)
        elif isinstance(message_set, int):
            message_buf = b'%d' % message_set

        args = b'%s %s %s' % (message_buf, command_buf, flags_buf)

        status, tag = self._process_command_UID(name, args)
        return status, tag, None

    def subscribe(self, mailbox: str) -> ResponseType:
        """
        Subscribe to a mailbox.

        This method sends the SUBSCRIBE command to the IMAP server to subscribe
        to the specified mailbox. Subscribing to a mailbox allows the client to
        receive notifications about new messages or changes in the subscribed
        mailbox.

        Parameters
        ----------
        mailbox
            The name of the mailbox to subscribe to.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Authenticated State;
        * The SUBSCRIBE command is used to subscribe to a specific mailbox on
          the server;
        * Subscribing to a mailbox enables the client to receive notifications
          about new messages or changes in the subscribed mailbox;
        * The server response to the SUBSCRIBE command indicates the success or
          failure of the subscription attempt;
        * RFC3501: [SUBSCRIBE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.6).
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'SUBSCRIBE'
        args = b'"%s"' % mailbox_buf

        status, tag = self._process_command(name, args)
        return status, tag, None

    def thread(self, thread_alg: str, charset: str, search_criteria: str) -> ResponseType:
        """
        Perform a threaded search using the THREAD IMAP command.

        The THREAD command is a variant of SEARCH with threading semantics for
        the results. It allows the client to organize search results into
        hierarchical threads based on message relationships, such as replies
        and references.

        This method first checks if the UIDPLUS extension is available on the
        server. If the UIDPLUS extension is available, it uses the "UID THREAD"
        command to perform the threaded search with message UIDs. Otherwise, it
        falls back to using the plain "THREAD" command without UIDs.

        Parameters
        ----------
        thread_alg
            The threading algorithm to use for organizing the search results
            into threads. Supported threading algorithms include "REFERENCES"
            and "ORDEREDSUBJECT".

        charset
            The character set to be used for sorting. This specifies the
            character encoding of the messages.

            Example:

            *  'utf-8': Use UTF-8 charset for the search;
            *  'us-ascii': Use US-ASCII charset for the search.

        search_criteria
            The search criteria to filter the messages before sorting. This can
            be any valid IMAP search criteria.

            See the [search criteria][imaplib2.imapp.IMAP4P.search] in the
            search method for examples of search criteria.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - list of message sequence numbers or of UIDs.
              The threads are represented with nested lists;

        Notes
        -----
        * The THREAD command is used to perform a threaded search based on
          specified criteria;
        * The method checks for the availability of the UIDPLUS extension to
          determine whether to use the "UID THREAD" command for threaded
          searches with UIDs;
        * The availability of the threading algorithms is server dependent, the
          following capabilities are tested:
            * THREAD=ORDEREDSUBJECT;
            * THREAD=REFERENCES;
        * RFC5256: [THREAD Command](https://datatracker.ietf.org/doc/html/rfc5256).

        Examples
        --------
        ```python
        >>> client.thread(thread_alg='REFERENCES',
                          charset='UTF-8',
                          search_criteria='FROM "example@example.com"')
        [166, 167, 168, 169, 172, [170, 179], 171, 173, [174, 175, 176, 178, 181, 180]]
        ```
        """
        if not self.has_capability('THREAD=REFERENCES') and thread_alg.upper() == 'REFERENCES':
            raise self.Error('The server does not support the THREAD=REFERENCES capability.')
        if (
            not self.has_capability('THREAD=ORDEREDSUBJECT')
            and thread_alg.upper() == 'ORDEREDSUBJECT'
        ):
            raise self.Error('The server does not support the THREAD=ORDEREDSUBJECT capability.')
        if self.has_uid():
            return self.thread_uid(thread_alg, charset, search_criteria)
        else:
            return self.thread_seq(thread_alg, charset, search_criteria)

    def thread_seq(self, thread_alg: str, charset: str, search_criteria: str) -> ResponseType:
        """
        This is the sequence version of the THREAD command. It returns unique
        sequence numbers.

        The server must support the THREAD= extension.

        Please see [IMAP4P.thread][imaplib2.imapp.IMAP4P.thread].
        """
        # Convert arguments to bytes
        thread_alg_buf = thread_alg.encode(self.encoding)
        charset_buf = charset.encode(self.encoding)
        search_criteria_buf = search_criteria.encode(self.encoding)

        name = b'THREAD'
        args = b'%s %s %s' % (thread_alg_buf, charset_buf, search_criteria_buf)

        self.sstatus['thread_response'] = []

        status, tag = self._process_command(name, args)
        return status, tag, self.sstatus['thread_response']

    def thread_uid(self, thread_alg: str, charset: str, search_criteria: str) -> ResponseType:
        """
        This is the UID version of the THREAD command. It returns message
        unique identifiers.

        The server must support the UIDPLUS extension and the THREAD= extension.

        Please see [IMAP4P.thread][imaplib2.imapp.IMAP4P.thread].
        """
        # Convert arguments to bytes
        thread_alg_buf = thread_alg.encode(self.encoding)
        charset_buf = charset.encode(self.encoding)
        search_criteria_buf = search_criteria.encode(self.encoding)

        if not self.has_capability('UIDPLUS'):
            raise self.Error('The server does not support the UIDPLUS capability.')

        name = b'THREAD'
        args = b'%s %s %s' % (thread_alg_buf, charset_buf, search_criteria_buf)

        self.sstatus['thread_response'] = []

        status, tag = self._process_command_UID(name, args)
        return status, tag, self.sstatus['thread_response']

    def uid(self, command: str, args: str) -> ResponseType:
        """
        Execute a UID-based IMAP command.

        This method is used to perform IMAP commands that operate on message
        UIDs. It constructs and sends a command string prefixed with "UID" to
        the server, indicating that the command should be interpreted in the
        context of message UIDs.

        In this library we have a collection of `_uid` commands that will use the
        UID version of the commands and a collection of `_seq` commands that will
        use the message sequence number commands. But the user should use the
        non specific version of these commands (without the `_seq` or `_uid`),
        these non-specific version will use the UID version if available
        falling back to the message sequence versions if the UID is not
        available.

        This method is provided for completeness sake.

        Parameters
        ----------
        command
            The base IMAP command to execute, such as "FETCH", "STORE", or
            "SEARCH".

        args
            Additional arguments or parameters for the command, formatted as a
            string.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Selected State;
        * The server must support the UIDPLUS capability (RFC 4315) to perform
          certain UID-based commands;
        * RFC3501: [UID Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.4.8).
        """
        # Convert arguments to bytes
        command_buf = command.encode(self.encoding)
        args_buf = args.encode(self.encoding)

        name = b'UID'
        args_pr = b'%s %s' % (command_buf, args_buf)

        status, tag = self._process_command(name, args_pr)
        return status, tag, None

    def unselect(self) -> ResponseType:
        """
        Unselects the currently selected mailbox.

        This method sends the UNSELECT command to the IMAP server, which
        unselects the currently selected mailbox without expunging it. Unlike
        the CLOSE command, the UNSELECT command leaves the mailbox open and
        authenticated, returning the client to the authenticated state.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * The UNSELECT command is an IMAP extension that allows the client to
          unselect the currently selected mailbox without expunging it.
        * After executing the UNSELECT command, the client remains
          authenticated and can continue interacting with the server without
          closing the connection.
        * RFC3691: [UNSELECT Command](https://datatracker.ietf.org/doc/html/rfc3691).
        """
        if not self.has_capability('UNSELECT'):
            raise self.Error('The server does not support the UNSELECT capability.')

        name = b'UNSELECT'

        try:
            status, tag = self._process_command(name)
        finally:
            self.state = 'AUTH'

        return status, tag, None

    def unsubscribe(self, mailbox: str) -> ResponseType:
        """
        Unsubscribe from a mailbox.

        This method sends the UNSUBSCRIBE command to the IMAP server,
        instructing it to remove the subscription for the specified mailbox.
        Unsubscribing from a mailbox means that the client will no longer
        receive notifications about new messages or changes in the unsubscribed
        mailbox.

        Parameters
        ----------
        mailbox : str
            The name of the mailbox from which to unsubscribe.

        Returns
        -------
        ResponseType
            This type is a tuple with three elements:

            * status:
                * 'OK' - command successful;
                * 'NO' - Error;
            * tag - the IMAP tag used;
            * command response - None for this command.

        Notes
        -----
        * Client Command - Authenticated State;
        * The UNSUBSCRIBE command is used to remove the subscription for a
          specific mailbox;
        * After unsubscribing from a mailbox, the client will no longer receive
          notifications about new messages or changes in the unsubscribed
          mailbox;
        * RFC3501: [UNSUBSCRIBE Command](https://datatracker.ietf.org/doc/html/rfc3501#section-6.3.7).
        """
        # Convert arguments to bytes
        mailbox_buf = encode_to_utf7(mailbox)

        name = b'UNSUBSCRIBE'
        args = b'"%s"' % (mailbox_buf)

        status, tag = self._process_command(name, args)
        return status, tag, None


if __name__ == '__main__':
    raise Exception('See src/imaplib2/examples/imapp_example.py')

# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""This module provides a single class that is used to register status
responses from the server.
"""

from typing import Any, Callable, List, TypedDict


class ActionType(TypedDict):
    etype: str
    action: Callable


class InfoLog(list):
    """Collects and manages the information and warnings issued by the server in
    the form of status responses.

    This can be overriden or completly replaced provided that the interface
    stays the same.

    By default it stores the last 10 entries, this can be defined while
    creating the instance.
    """

    def __init__(self, max_entries: int = 10, *args):
        """Creates a new InfoLog list.

        Parameters
        ----------
        max_entries
            Number of entries to keep
        """
        self.max_entries = max_entries
        self.action_list: List[ActionType] = []

        list.__init__(self, *args)

    def add_entry(self, etype: str, data: Any) -> None:
        """Adds a new log entry.

        Parameters
        ----------
        etype
            The type of the entry (warning, error, info, etc)
        data
            any python object.
        """
        etype = etype.upper()
        if len(self) == self.max_entries:
            del self[0]
        self.append({'type': etype, 'data': data})

        for action in self.action_list:
            if action['etype'] == etype:
                action['action'](etype, data)

    def add_action(self, etype: str, action: Callable) -> None:
        """A callback action can be defined. Every time a new log is made
        the callback action will be executed.

        Parameters
        ----------
        etype
            The type that will trigger the action
        action
            A python callable, the arguments used will be (etype, data).
        """
        self.action_list.append({'etype': etype.upper(), 'action': action})


if __name__ == '__main__':

    def printAA(etype, data):
        print('Type: ', etype)
        print('Data: :', data)

    info_log = InfoLog()
    info_log.add_action('AA', printAA)

    for i in range(20):
        info_log.add_entry('AA', 'AAAAA %d' % i)

    for entry in info_log:
        print(entry['data'])

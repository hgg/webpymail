#!/usr/bin/env python3

from time import time

from imaplib2.sexp import scan_sexp

data = (
    b'266 FETCH (FLAGS (\\Seen) UID 31608 INTERNALDATE '
    b'"30-Jan-2008 02:48:01 +0000" RFC822.SIZE 4509 ENVELOPE '
    b'("Tue, 29 Jan 2008 14:00:24 +0000" "Example subject '
    b'for usage in this test..." (("Sender" NIL "sender" '
    b'"example.org")) (("Sender" NIL "sender" "example.org"))'
    b' ((NIL NIL "sender" "example.org")) ((NIL NIL "helder" '
    b'"example.com")) NIL NIL NIL "<293487204987234098adasd3213@'
    b'localhost.localdomain>"))'
)

print('Test to the s-exp parser:\n')

itx = 1000

print(f'Non Recursive ({itx} times):')

a = time()
for i in range(itx):
    scan_sexp(data)
b = time()

print(1000 * (b - a) / itx, 'ms/iter')
print(itx, ' --> ', 1000 * (b - a), 'ms')
print()
print('Scanned result:')
print(scan_sexp(data))

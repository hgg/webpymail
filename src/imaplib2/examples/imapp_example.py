#!/usr/bin/env python3

# imaplib2 python module, meant to be a replacement to the python default
# imaplib module
# Copyright (C) 2008 Helder Guerreiro

# This file is part of imaplib2.
#
# imaplib2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# imaplib2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

"""Example usage of imaplib2.imaplp"""

import datetime
import getopt
import getpass
import sys
from email.message import EmailMessage

import imaplib2.imapp
import pytz

imaplib2.imapp.Debug = 3

if __name__ == '__main__':
    try:
        optlist, args = getopt.getopt(sys.argv[1:], 'd:s:')
    except getopt.error:
        optlist, args = (), ()

    if not args:
        args = ('',)

    host = args[0]

    # getuser checks the environment variables LOGNAME, USER, LNAME and
    # USERNAME, in this order, and returns the value of the first one which is
    # set to a non-empty string.
    USER = getpass.getuser()
    PASSWD = getpass.getpass('IMAP password for %s on %s: ' % (USER, host or 'localhost'))

    print(f'{USER=}')

    # Login establish the connection to the server
    M = imaplib2.imapp.IMAP4P(host, port=993, ssl=True)

    # Login to the server
    M.login(USER, PASSWD)

    # APPEND command
    dt = datetime.datetime(2024, 2, 29, 15, 15, 15, tzinfo=pytz.utc)
    msg = EmailMessage()
    msg['from'] = 'example1@example.org'
    msg['to'] = 'example2@example.org'
    msg['subject'] = 'Test message 2'
    msg['date'] = dt
    msg.set_content('Hello, do you think we can meet tomorrow at 3:15?')

    M.append('INBOX', msg.as_bytes(), ['Flag1', 'Flag2'], dt)

    sys.exit()

    print(M.list('INBOX', '*'))
    print(M.examine('INBOX'))
    print(M.examine('INBOX.Drafts'))
    print('Close: ', M.close())
    print()

    for folder in M.list('INBOX', '*'):
        print(folder)

    # Select a folder
    M.select('INBOX.Templates')

    ml = M.search_uid('ALL')

    a = M.fetch_uid(ml)
    print(len(list(a.keys())))

# hlimap - High level IMAP library
# Copyright (C) 2008 Helder Guerreiro

# This file is part of hlimap.
#
# hlimap is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hlimap is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hlimap.  If not, see <http://www.gnu.org/licenses/>.

#
# Helder Guerreiro <helder@tretas.org>
#

import socket
from typing import Optional

from imaplib2.imapp import IMAP4P, ResponseType

from hlimap.imapfolder import FolderTree

# Exceptions


class NoFolderListError(Exception):
    pass


class NoSuchFolder(Exception):
    pass


class ServerError(Exception):
    pass


# IMAP Server


class ImapServer:
    """
    Establishes the server connection, and does the authentication.
    """

    def __init__(
        self,
        host: str = 'localhost',
        port: Optional[int] = None,
        ssl: bool = False,
        keyfile: Optional[str] = None,
        certfile: Optional[str] = None,
    ):
        """
        Parameters
        ----------

        host
            host name of the imap server;
        port
            port to be used. If not specified it will default to 143 for plain
            text and 993 for ssl;
        ssl
            Is the connection ssl?
        keyfile
            PEM formatted private key;
        certfile
            certificate chain file for the SSL connection.
        """

        try:
            self._imap = IMAP4P(
                host=host, port=port, ssl=ssl, keyfile=keyfile, certfile=certfile, autologout=False
            )
            self.connected = True
        except socket.gaierror:
            self.connected = False
            raise

        self.special_folders: list[str] = []
        self.expand_list: tuple[str] = ()
        self.__folder_tree = None
        # Available iterators:
        # 'iter_all' - iterate through all folders
        # 'iter_expand' - iterate through all the folders that are on the expand_list
        self.folder_iterator = 'iter_expand'

    # IMAP methods
    def login(self, username: str, password: str) -> ResponseType:
        """
        Performs the login on the server.

        Parameters
        ----------
        username
            The username to log in with.
        password
            The password corresponding to the user.

        Returns
        -------
        IMAP4P
            it returns the LOGIN imap4 command response on the format defined
            on the imaplib2 library.
        """
        return self._imap.login(username, password)

    # Folder list management

    def set_special_folders(self, *folder_list):
        """
        The folders are displayed always with 'INBOX' in the first place, then
        all the folders defined on this method, and then the folders on the
        order returned by the server.
        """
        # TODO: We should try to get the special folders from the list response
        #       * LIST (\Subscribed \HasNoChildren \sent) "/" Sent
        self.special_folders = folder_list

    def set_expand_list(self, *folder_list: str) -> None:
        """From which folders should we fetch the sub folder list?"""
        self.expand_list = folder_list

    def _get_folder_tree(self) -> FolderTree:
        if not self.__folder_tree:
            self.__folder_tree = FolderTree(self)
        return self.__folder_tree

    folder_tree = property(_get_folder_tree)

    def refresh_folders(self, subscribed=True):
        """This method extracts the folder list from the
        server.
        """
        self.folder_tree.refresh_folders(subscribed)

        self.folder_tree.set_properties(self.expand_list, self.special_folders)

        self.folder_tree.sort()

        self.set_folder_iterator()

    def set_folder_iterator(self) -> None:
        """
        Predefines the iterator to use on the folder tree. The available
        iterators are defined on FolderTree
        """
        try:
            it = getattr(self.folder_tree, self.folder_iterator)
        except AttributeError:
            raise ServerError('Unknown iterator "%s"' % self.folder_iterator)

        if self.folder_tree:
            self.folders = it
        else:
            raise NoFolderListError('No folder list')

    # Special methods
    def __del__(self) -> None:
        """Logs out from the imap server when the class instance is deleted"""
        if self.connected:
            self._imap.logout()

    def __getitem__(self, path):
        """Returns a folder object"""
        if not self.folder_tree:
            self.folder_tree = FolderTree(self)
        if isinstance(path, bytes):
            path = str(path, 'ascii')
        return self.folder_tree.get_folder(path)

    def __iter__(self):
        """Iteracts through the folders"""
        if not self.folder_tree or not hasattr(self, 'folders'):
            self.refresh_folders()

        return self.folders()

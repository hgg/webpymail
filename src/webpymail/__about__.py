# SPDX-FileCopyrightText: 2023-present Helder Guerreiro <helder@tretas.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
__version__ = '0.2.0'

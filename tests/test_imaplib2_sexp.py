#!/usr/bin/env python3

import pytest
from imaplib2.sexp import flatten_sexp, scan_sexp

test_data = [
    (
        (
            b'* OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE AUTH=PLAIN AUTH=LOGIN SASL-IR] '
            b'server Cyrus IMAP 3.4.5 server ready'
        ),
        [
            b'*',
            b'OK',
            b'[CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE AUTH=PLAIN AUTH=LOGIN SASL-IR]',
            b'server',
            b'Cyrus',
            b'IMAP',
            b'3.4.5',
            b'server',
            b'ready',
        ],
    ),
    (
        b'(A NIL {5}\r\n12345 (D E))(F G)',
        [[b'A', b'NIL', b'12345', [b'D', b'E']], [b'F', b'G']],
    ),
    (
        (
            b'266 FETCH (FLAGS (\\Seen) UID 31608 INTERNALDATE '
            b'"30-Jan-2008 02:48:01 +0000" RFC822.SIZE 4509 ENVELOPE '
            b'("Tue, 29 Jan 2008 14:00:24 +0000" "Example subject '
            b'for usage in this test..." (("Sender" NIL "sender" '
            b'"example.org")) (("Sender" NIL "sender" "example.org"))'
            b' ((NIL NIL "sender" "example.org")) ((NIL NIL "helder" '
            b'"example.com")) NIL NIL NIL "<293487204987234098adasd3213@'
            b'localhost.localdomain>"))'
        ),
        [
            b'266',
            b'FETCH',
            [
                b'FLAGS',
                [b'\\Seen'],
                b'UID',
                b'31608',
                b'INTERNALDATE',
                b'30-Jan-2008 02:48:01 +0000',
                b'RFC822.SIZE',
                b'4509',
                b'ENVELOPE',
                [
                    b'Tue, 29 Jan 2008 14:00:24 +0000',
                    b'Example subject for usage in this test...',
                    [[b'Sender', b'NIL', b'sender', b'example.org']],
                    [[b'Sender', b'NIL', b'sender', b'example.org']],
                    [[b'NIL', b'NIL', b'sender', b'example.org']],
                    [[b'NIL', b'NIL', b'helder', b'example.com']],
                    b'NIL',
                    b'NIL',
                    b'NIL',
                    b'<293487204987234098adasd3213@localhost.localdomain>',
                ],
            ],
        ],
    ),
    (b'{8}\r\n1234\xc3\xa3\r\n6', [b'1234\xc3\xa3\r\n', b'6']),
    (
        (
            b'* 8444 FETCH (UID 263313 BODY[1] {183}\r\n<!DOCTYPE html>\r\n<html>\r\n  '
            b'<head>\r\n\r\n    <meta http-equiv="content-type" content="data/html; '
            b'charset=UTF-8">\r\n  </head>\r\n  <body>\r\n    <p>C\xc3\xa3o C\xc3\xa3o<br>\r\n    '
            b'</p>\r\n  </body>\r\n</html>\r\n)'
        ),
        [
            b'*',
            b'8444',
            b'FETCH',
            [
                b'UID',
                b'263313',
                b'BODY[1]',
                (
                    b'<!DOCTYPE html>\r\n<html>\r\n  <head>\r\n\r\n    <meta http-equiv='
                    b'"content-type" content="data/html; charset=UTF-8">\r\n  </head>\r\n'
                    b'  <body>\r\n    <p>C\xc3\xa3o C\xc3\xa3o<br>\r\n    </p>\r\n  </body>'
                    b'\r\n</html>\r\n'
                ),
            ],
        ],
    ),
]


@pytest.mark.parametrize('data, expected', test_data)
def test_scan_sexp(data: bytes, expected: list):
    result = scan_sexp(data)
    print('The computed result was:')
    print(result)
    assert result == expected


def test_flatten_sexp():
    # Test case 1: Empty list
    assert list(flatten_sexp([])) == []

    # Test case 2: List with single byte elements
    nested_list_1 = [b'a', b'b', b'c']
    assert list(flatten_sexp(nested_list_1)) == [b'a', b'b', b'c']

    # Test case 3: Nested list with single byte elements
    nested_list_2 = [b'a', [b'b', [b'c', b'd'], b'e'], b'f']
    assert list(flatten_sexp(nested_list_2)) == [b'a', b'b', b'c', b'd', b'e', b'f']

    # Test case 4: Nested list with nested empty lists
    nested_list_3 = [b'a', [], [b'b', [[]], [b'c', b'd'], []], b'e', []]
    assert list(flatten_sexp(nested_list_3)) == [b'a', b'b', b'c', b'd', b'e']

    # Test case 5: Nested list with mixed byte elements and nested lists
    nested_list_4 = [b'a', [b'b', [b'c', [b'd', [b'e']], b'f']], b'g']
    assert list(flatten_sexp(nested_list_4)) == [b'a', b'b', b'c', b'd', b'e', b'f', b'g']

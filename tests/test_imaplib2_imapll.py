#!/usr/bin/env python3

import unittest.mock

from imaplib2.imapll import IMAP4


# Mock the open method
def open(self, host, port):
    self.mock_sock = unittest.mock.Mock()
    # Assign the mock socket and abort objects to the instance attributes
    self.sock = self.mock_sock
    self.file = self.mock_sock
    self.file.readline.return_value = (
        b'* OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE AUTH=PLAIN '
        b'AUTH=LOGIN SASL-IR] optiosrv Cyrus IMAP 2.5.10-Debian-2.5'
        b'.10-3+deb9u3 server ready\r\n'
    )


def test_command():
    # Create an instance of your class that has the send method
    IMAP4.open = open
    imap = IMAP4('localhost')
    # Call your method with some input data
    data = b'Hello'
    imap.send(data)
    # Assert that the mock socket object was called with the correct data
    imap.mock_sock.sendall.assert_called_with(data)


def test_send_command():
    IMAP4.open = open
    imap = IMAP4('localhost')
    data = b'CAPABILITY'
    tag = b'%s000' % imap.tagpre
    imap.file.readline.side_effect = (
        b'* CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL RIGHTS=kxten QUOTA MAILBOX-REFERRALS NAMES'
        b'PACE UIDPLUS NO_ATOMIC_RENAME UNSELECT CHILDREN MULTIAPPEND BINARY CATENATE CONDSTORE ES'
        b'EARCH SORT SORT=MODSEQ SORT=DISPLAY SORT=UID THREAD=ORDEREDSUBJECT THREAD=REFERENCES ANN'
        b'OTATEMORE ANNOTATE-EXPERIMENT-1 METADATA LIST-EXTENDED LIST-STATUS LIST-MYRIGHTS WITHIN '
        b'QRESYNC SCAN XLIST XMOVE MOVE SPECIAL-USE CREATE-SPECIAL-USE URLAUTH URLAUTH=BINARY AUTH'
        b'=PLAIN AUTH=LOGIN SASL-IR COMPRESS=DEFLATE X-QUOTA=STORAGE X-QUOTA=MESSAGE X-QUOTA=X-ANN'
        b'OTATION-STORAGE X-QUOTA=X-NUM-FOLDERS IDLE\r\n',
        b'%s OK Completed\r\n' % tag,
    )
    tag, result = imap.send_command(data)
    assert result['tagged'][tag]['status'] == b'OK'

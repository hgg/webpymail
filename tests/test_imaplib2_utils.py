#!/usr/bin/env python3

import pytest
from imaplib2.utils import (
    UTF7Error,
    decode_utf7,
    encode_to_utf7,
    list_to_bytes,
    list_to_int,
    list_to_str,
    shrink_fetch_list,
    unquote,
)

test_data_unquote = (
    (b"'ABC'", b'ABC'),
    (b'"ABC"', b'ABC'),
    (b'"ABC', b'"ABC'),
    (b'ABC"', b'ABC"'),
)


@pytest.mark.parametrize('data, expected', test_data_unquote)
def test_unquote(data: bytes, expected: list):
    result = unquote(data)
    print(f'The computed result was: {result}')
    assert result == expected


test_data_shrink = (
    ([1, 2, 3, 4], [b'1:4']),
    ([1, 2, 3, 4, 10, 12, 23, 24, 25], [b'1:4', b'10', b'12', b'23:25']),
    ([1, 3, 5, 7, 9], [b'1', b'3', b'5', b'7', b'9']),
)


@pytest.mark.parametrize('data, expected', test_data_shrink)
def test_shrink(data: list[int], expected: list[bytes]) -> None:
    result = shrink_fetch_list(data)
    print(f'The computed result was: {result}')
    assert result == expected


test_data_encode = (
    # Test basic conversion
    ('hello', b'hello'),
    # Test conversion with special characters
    ('€$&', b'&IKw-$&-'),
    # Test conversion with ASCII characters and special characters
    ('hello & world', b'hello &- world'),
    # Test conversion with non-ASCII characters and special characters
    ('你好&world', b'&T2BZfQ-&-world'),
    ('cão', b'c&AOM-o'),
    # Test conversion with empty string
    ('', b''),
    # Test wildcards % and *
    ('*%', b'*%'),
)


@pytest.mark.parametrize('data, expected', test_data_encode)
def test_encode_to_utf7(data: str, expected: bytes) -> None:
    result = encode_to_utf7(data)
    print(f'The computed result was: {result}')
    assert result == expected


test_data_decode = (
    # Test basic decoding
    (b'hello', 'hello'),
    # Test decoding with special characters
    (b'&IKw-$&-', '€$&'),
    # Test decoding with ASCII characters and special characters
    (b'hello &- world', 'hello & world'),
    # Test decoding with non-ASCII characters and special characters
    (b'&T2BZfQ-&-world', '你好&world'),
    # Test decoding with empty string
    (b'', ''),
)


@pytest.mark.parametrize('data, expected', test_data_decode)
def test_decode_utf7(data: bytes, expected: str) -> None:
    result = decode_utf7(data)
    print(f'The computed result was: {result}')
    assert result == expected


def test_decode_errors():
    # Test decoding with incomplete encoding (ending with &)
    with pytest.raises(UTF7Error):
        decode_utf7(b'&')

    # Test decoding with invalid utf-7 sequence (non terminated base64 seq)
    with pytest.raises(UTF7Error):
        decode_utf7(b'&asdasd')

    # Test decoding with invalid utf-7 sequence (invalid character in base64 seq)
    with pytest.raises(UTF7Error):
        decode_utf7(b'&asd#asd')

    with pytest.raises(UTF7Error):
        decode_utf7(b'&#')


# List_to_int

test_data_list_to_int = (
    # Test conversion of a single value
    (['123'], [123]),
    # Test conversion of a flat list
    (['234', '127', '78345', '0'], [234, 127, 78345, 0]),
    # Test conversion of a nested list
    ([['1', '2'], ['3', '4']], [[1, 2], [3, 4]]),
    (['234', ['127', '78345', ['345', '9455']], ['0']], [234, [127, 78345, [345, 9455]], [0]]),
    # Test conversion of a mixed list with strings and integers
    (['1', ['2', '3'], '4'], [1, [2, 3], 4]),
    # Test conversion of an empty list
    ([], []),
)


@pytest.mark.parametrize('data, expected', test_data_list_to_int)
def test_list_to_int(data, expected):
    result = list_to_int(data)
    assert result == expected


def test_list_to_int_error():
    # Test conversion with invalid input (non-integer strings)
    with pytest.raises(ValueError):
        list_to_int(['abc'])

    # Test conversion with nested invalid input (non-integer strings)
    with pytest.raises(ValueError):
        list_to_int([['1', '2'], ['3', 'abc']])


# list_to_str - convert list of bytes to list of str

test_data_list_to_str = (
    # Test conversiont of a flat list
    ([b'hello', b'world'], ['hello', 'world']),
    # Test conversiont of a nested list
    ([[b'hello', b'world'], [b'foo', b'bar']], [['hello', 'world'], ['foo', 'bar']]),
    ([b'hello', [b'world'], b'foo'], ['hello', ['world'], 'foo']),
    # Test conversion of an empty list
    ([], []),
)


@pytest.mark.parametrize('data, expected', test_data_list_to_str)
def test_list_to_str(data, expected):
    result = list_to_str(data)
    assert result == expected


def test_list_to_str_error():
    with pytest.raises(UnicodeDecodeError):
        list_to_str([b'\xff\xff\xff'], encoding='utf-8')


# list_to_bytes - convert list of strings to list of bytes


test_data_list_to_bytes = (
    # Flat list
    (['hello', 'world'], [b'hello', b'world']),
    # Nested
    (['hello', ['world', 'python']], [b'hello', [b'world', b'python']]),
    # Empty list
    ([], []),
    # Encoding flat
    (
        ['привет', 'мир'],
        [b'\xd0\xbf\xd1\x80\xd0\xb8\xd0\xb2\xd0\xb5\xd1\x82', b'\xd0\xbc\xd0\xb8\xd1\x80'],
    ),
    # Encoding nested
    (
        ['hello', ['привет', 'мир']],
        [
            b'hello',
            [b'\xd0\xbf\xd1\x80\xd0\xb8\xd0\xb2\xd0\xb5\xd1\x82', b'\xd0\xbc\xd0\xb8\xd1\x80'],
        ],
    ),
)


@pytest.mark.parametrize('data, expected', test_data_list_to_bytes)
def test_list_to_bytes(data, expected):
    result = list_to_bytes(data)
    assert result == expected

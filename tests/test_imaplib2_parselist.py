from imaplib2.parselist import HASCHILDREN, HASNOCHILDREN, NOSELECT, Mailbox
from imaplib2.utils import decode_utf7

# Test data
path = b'INBOX'
attributes = [NOSELECT, HASCHILDREN]
delimiter = b'/'


# Test cases for Mailbox class
def test_mailbox_init():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.path == decode_utf7(path)
    assert mailbox.attributes == tuple(attributes)
    assert mailbox.delimiter == delimiter.decode('utf-8')
    assert mailbox.parts == ('INBOX',)


def test_mailbox_test_attribute():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.test_attribute(NOSELECT)
    assert mailbox.test_attribute(HASCHILDREN)
    assert not mailbox.test_attribute(HASNOCHILDREN)


def test_mailbox_noselect():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.noselect()


def test_mailbox_has_children():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.has_children()


def test_mailbox_str():
    mailbox = Mailbox(path, attributes, delimiter)
    assert str(mailbox) == 'INBOX'


def test_mailbox_eq():
    mailbox1 = Mailbox(path, attributes, delimiter)
    mailbox2 = Mailbox(path, attributes, delimiter)
    assert mailbox1 == mailbox2


def test_mailbox_level():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.level() == 0


def test_mailbox_last_level():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.last_level() == 'INBOX'


def test_mailbox_native():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.native() == 'INBOX'


def test_mailbox_url():
    mailbox = Mailbox(path, attributes, delimiter)
    assert mailbox.url() == 'SU5CT1g='


def test_mailbox_repr():
    mailbox = Mailbox(path, attributes, delimiter)
    assert repr(mailbox) == '<Mailbox instance "INBOX">'

import unittest.mock

import hlimap
from imaplib2.imapll import IMAP4


# Mock the open method
def open(self, host, port):
    self.mock_sock = unittest.mock.Mock()
    # Assign the mock socket and abort objects to the instance attributes
    self.sock = self.mock_sock
    self.file = self.mock_sock
    self.file.readline.return_value = (
        b'* OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE AUTH=PLAIN '
        b'AUTH=LOGIN SASL-IR UIDPLUS ACL NAMESPACE SORT THREAD=REFE'
        b'RENCES THREAD=ORDEREDSUBJECT UNSELECT] optiosrv Cyrus IMA'
        b'P 2.5.10-Debian-2.5.10-3+deb9u3 server ready\r\n'
    )


def test_imapserver_instantiate():
    IMAP4.open = open
    server = hlimap.ImapServer('example.org', port=143, ssl=False)
    tagpre = server._imap._IMAP4P__IMAP4.tagpre
    server._imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL]\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    status, tag, _ = server.login('user', 'passwd')
    assert server.connected
    assert status == 'OK'


def test_imapserver_create_foldertree():
    IMAP4.open = open
    server = hlimap.ImapServer('example.org', port=143, ssl=False)
    tagpre = server._imap._IMAP4P__IMAP4.tagpre
    server._imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL]\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    status, tag, _ = server.login('user', 'passwd')
    print(server.folder_tree)
    assert server.connected
    assert status == 'OK'

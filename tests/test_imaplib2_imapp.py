import datetime
import unittest.mock
from email.message import EmailMessage, Message
from pprint import pprint

import imaplib2
import pytest
import pytz
from imaplib2.imapll import IMAP4
from imaplib2.imapp import IMAP4P
from imaplib2.sexp import flatten_sexp
from imaplib2.utils import list_to_str


# Mock the open method
def open(self, host, port):
    self.mock_sock = unittest.mock.Mock()
    # Assign the mock socket and abort objects to the instance attributes
    self.sock = self.mock_sock
    self.file = self.mock_sock
    self.file.readline.return_value = (
        b'* OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE AUTH=PLAIN '
        b'AUTH=LOGIN SASL-IR UIDPLUS ACL NAMESPACE SORT THREAD=REFE'
        b'RENCES THREAD=ORDEREDSUBJECT UNSELECT] optiosrv Cyrus IMA'
        b'P 2.5.10-Debian-2.5.10-3+deb9u3 server ready\r\n'
    )


def test_utility_has_capability():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
    )
    assert imap.has_capability('IMAP4rev1')
    assert not imap.has_capability('INEXISTENT')


def test_command_append():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK APPEND Completed (0.000 sec)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    msg = EmailMessage()
    msg.set_content('cão')
    message = msg.as_bytes()
    dt = datetime.datetime(2024, 2, 29, 15, 15, 15, tzinfo=pytz.utc)
    result = imap.append('INBOX', message, ['Flag1', 'Flag2'], dt)
    append_log = imap.infolog[-1]
    tag = (b'%s000' % (tagpre)).decode('utf-8')

    assert result[1] == tag
    assert result[0] == 'OK'

    print('Command response:')
    pprint(result)
    print('Response message:')
    print(' '.join(list_to_str(flatten_sexp(append_log['data']['message']))))


def test_command_capability():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL RIGHTS=kxten QUOTA MAILBOX-REFERRALS NAMES'
        b'PACE UIDPLUS NO_ATOMIC_RENAME UNSELECT CHILDREN MULTIAPPEND BINARY CATENATE CONDSTORE ES'
        b'EARCH SORT SORT=MODSEQ SORT=DISPLAY SORT=UID THREAD=ORDEREDSUBJECT THREAD=REFERENCES ANN'
        b'OTATEMORE ANNOTATE-EXPERIMENT-1 METADATA LIST-EXTENDED LIST-STATUS LIST-MYRIGHTS WITHIN '
        b'QRESYNC SCAN XLIST XMOVE MOVE SPECIAL-USE CREATE-SPECIAL-USE URLAUTH URLAUTH=BINARY AUTH'
        b'=PLAIN AUTH=LOGIN SASL-IR COMPRESS=DEFLATE X-QUOTA=STORAGE X-QUOTA=MESSAGE X-QUOTA=X-ANN'
        b'OTATION-STORAGE X-QUOTA=X-NUM-FOLDERS IDLE\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    status, tag, capability = imap.capability()

    response = [
        'IMAP4REV1',
        'LITERAL+',
        'ID',
        'ENABLE',
        'ACL',
        'RIGHTS=KXTEN',
        'QUOTA',
        'MAILBOX-REFERRALS',
        'NAMESPACE',
        'UIDPLUS',
        'NO_ATOMIC_RENAME',
        'UNSELECT',
        'CHILDREN',
        'MULTIAPPEND',
        'BINARY',
        'CATENATE',
        'CONDSTORE',
        'ESEARCH',
        'SORT',
        'SORT=MODSEQ',
        'SORT=DISPLAY',
        'SORT=UID',
        'THREAD=ORDEREDSUBJECT',
        'THREAD=REFERENCES',
        'ANNOTATEMORE',
        'ANNOTATE-EXPERIMENT-1',
        'METADATA',
        'LIST-EXTENDED',
        'LIST-STATUS',
        'LIST-MYRIGHTS',
        'WITHIN',
        'QRESYNC',
        'SCAN',
        'XLIST',
        'XMOVE',
        'MOVE',
        'SPECIAL-USE',
        'CREATE-SPECIAL-USE',
        'URLAUTH',
        'URLAUTH=BINARY',
        'AUTH=PLAIN',
        'AUTH=LOGIN',
        'SASL-IR',
        'COMPRESS=DEFLATE',
        'X-QUOTA=STORAGE',
        'X-QUOTA=MESSAGE',
        'X-QUOTA=X-ANNOTATION-STORAGE',
        'X-QUOTA=X-NUM-FOLDERS',
        'IDLE',
    ]

    print('Command response:')
    pprint([status, tag, capability])
    assert response == capability


def test_command_check():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    with pytest.raises(
        imaplib2.imapp.IMAP4P.Error, match="command b'CHECK' illegal in state 'NONAUTH'"
    ):
        status, tag, _ = imap.check()


def test_command_close():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    # imap.state = 'SELECTED'
    with pytest.raises(
        imaplib2.imapp.IMAP4P.Error, match="command b'CLOSE' illegal in state 'NONAUTH'"
    ):
        status, tag, _ = imap.close()


def test_command_copy_seq():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK [COPYUID 1706551508 35:36 1:2] Completed' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    imap.copy_seq([13, 14], 'Test')
    print('Command response:')
    print(f"{imap.sstatus['copy_response']=}")
    assert imap.sstatus['copy_response'] == ('1706551508', '35:36', '1:2')


def test_command_copy_uid():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK [COPYUID 1714768406 7,25,30:33 1:6] Completed' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.copy_uid([7, 25, 30, 31, 32, 33], 'Test')
    print('Command response:')
    print(f"{imap.sstatus['copy_response']=}")
    assert imap.sstatus['copy_response'] == ('1714768406', '7,25,30:33', '1:6')


def test_command_create():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK [MAILBOXID (213c5ca3-1b31-4d3d-aff7-4e1f9133e50f)] Completed\r\n'
        % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.create('Test')
    print('Command response:')
    pprint([status, tag, response])
    assert response['MAILBOXID'] == '213c5ca3-1b31-4d3d-aff7-4e1f9133e50f'


def test_command_create_no_objectid():
    """Test the CREATE command server without the OBJECTID extension"""
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.create('Test')
    print('Command response:')
    pprint([status, tag, response])
    assert response == {}
    assert status == 'OK'


def test_command_delete():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, _ = imap.delete('Test')
    print('Command response:')
    pprint([status, tag])
    assert status == 'OK'


def test_command_delete_error():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s NO Mailbox does not exist\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, _ = imap.delete('Test')
    print('Command response:')
    pprint([status, tag])
    assert status == 'NO'


def test_command_deleteacl():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, _ = imap.deleteacl('INBOX', 'cyrus')
    assert status == 'OK'


def test_command_examine():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 4 EXISTS\r\n',
        b'* 0 RECENT\r\n',
        b'* FLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen)\r\n',
        b'* OK [PERMANENTFLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen \\*)] Ok\r\n',
        b'* OK [UNSEEN 2] Ok\r\n',
        b'* OK [UIDVALIDITY 1682284751] Ok\r\n',
        b'* OK [UIDNEXT 5] Ok\r\n',
        b'* OK [HIGHESTMODSEQ 219] Ok\r\n',
        b'* OK [MAILBOXID (08746f42-4abc-425e-abab-c791533ce761)] Ok\r\n',
        b'* OK [URLMECH INTERNAL] Ok\r\n',
        b'* OK [ANNOTATIONS 65536] Ok\r\n',
        b'%s%s OK [READ-ONLY] Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    imap.examine('INBOX')
    print('Command response:')
    pprint(imap.sstatus['current_folder'])
    assert imap.sstatus['current_folder']['name'] == 'INBOX'
    assert imap.sstatus['current_folder']['is_readonly']


def test_command_examine_error():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s NO Mailbox does not exist\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, _ = imap.examine('INBOX')
    assert status == 'NO'
    assert imap.state == 'AUTH'


def test_command_expunge():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 10 EXPUNGE\r\n',
        b'* 10 EXPUNGE\r\n',
        b'%s%s OK [HIGHESTMODSEQ 446] Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.expunge()
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_fetch_seq_BODY():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 8 FETCH (BODY ("TEXT" "PLAIN" ("CHARSET" "utf-8") NIL NIL "7BIT" 51 1))\r\n',
        b'* 9 FETCH (BODY (("TEXT" "PLAIN" ("CHARSET" "UTF-8" "FORMAT" "flowed") NIL NIL "7BIT" '
        b'19 1)("TEXT" "HTML" ("CHARSET" "UTF-8") NIL NIL "7BIT" 196 11) "ALTERNATIVE"))\r\n',
        b'%s%s OK Completed (0.000 sec)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, result = imap.fetch_seq([8, 9], 'BODY')
    msg_id_8 = result[8]['BODY']
    msg_id_9 = result[9]['BODY']
    print('ID=8', msg_id_8, type(msg_id_8))
    print('ID=9', msg_id_9, type(msg_id_9))
    assert msg_id_8.is_plain()
    assert msg_id_8.is_text()
    assert msg_id_9.is_multipart()


def test_command_fetch_uid_BODY_ENVELOPE_INTERNALDATE():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 9 FETCH (UID 30 INTERNALDATE "28-Apr-2024 19:46:16 +0100" ENVELOPE ("Sun, 28 Apr 2024 '
        b'19:46:15 +0100" "TEST" (("Test User" NIL "apagar" "example.org")) (("Test User" NIL "apa'
        b'gar" "example.org")) (("Test User" NIL "apagar" "example.org")) (("Helder" NIL "helder" '
        b'"example.org")) NIL NIL NIL "<2dfc9d4e-79d8-4553-a484-c5d19060e585@example.org>") BODY ('
        b'("TEXT" "PLAIN" ("CHARSET" "UTF-8" "FORMAT" "flowed") NIL NIL "7BIT" 19 1)("TEXT" "HTML"'
        b' ("CHARSET" "UTF-8") NIL NIL "7BIT" 196 11) "ALTERNATIVE"))\r\n',
        b'%s%s OK Completed (0.000 sec)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, result = imap.fetch_uid([30], 'ENVELOPE BODY INTERNALDATE')
    msg_uid_30 = result[30]

    print('UID=30', msg_uid_30['BODY'], type(msg_uid_30['BODY']))
    print('Result:')
    pprint(result)
    print('Message structure:')
    print(msg_uid_30['BODY'].represent()[:-1])
    print('Envelope:')
    print('From:', list(msg_uid_30['ENVELOPE'].from_short()))
    print('To:', list(msg_uid_30['ENVELOPE'].to_short()))
    print('CC:', list(msg_uid_30['ENVELOPE'].cc_short()))

    assert msg_uid_30['BODY'].is_multipart()
    assert list(msg_uid_30['ENVELOPE'].from_short()) == ['Test User']
    assert list(msg_uid_30['ENVELOPE'].to_short()) == ['Helder']
    assert list(msg_uid_30['ENVELOPE'].cc_short()) == []


def test_command_fetch_uid_COMPLETE_MESSAGE():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 12 FETCH (FLAGS (\\Seen) BODY[] {441}\r\n'
        b'Message-ID: <7a0635d5-dcb1-4a3c-8464-083c6435c500@paxjulia.org>\r\n'
        b'Date: Wed, 1 May 2024 15:57:48 +0100\r\n'
        b'MIME-Version: 1.0\r\n'
        b'User-Agent: Thunderbird Daily\r\n'
        b'Content-Language: en-US\r\n'
        b'X-Priority: 1 (Highest)\r\n'
        b'To: Helder <helder@paxjulia.org>\r\n'
        b'From: User Teste <apagar@paxjulia.org>\r\n'
        b'Subject: Test message PLAIN\r\n'
        b'Content-Type: text/plain; charset=UTF-8; format=flowed\r\n'
        b'Content-Transfer-Encoding: 8bit\r\n'
        b'\r\n'
        b'Hello World!\r\n'
        b'\r\n'
        b'\xe3\x81\x93\xe3\x82\x93\xe3\x81\xab\xe3\x81\xa1\xe3\x81\xaf\xe4\xb8\x96\xe7\x95\x8c\xef'
        b'\xbc\x81\r\n'
        b'\r\n'
        b')\r\n',
        b'%s%s OK Completed (0.000 sec)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, result = imap.fetch_seq([12], 'FLAGS BODY[]')
    print(result)
    assert result[12]['FLAGS'][0] == '\\Seen'
    assert isinstance(result[12]['BODY[]'], Message)


def test_command_getacl():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* ACL INBOX user1 lrswipkxtecdan\r\n',
        b'* ACL INBOX user2 lrs\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.getacl('INBOX')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_list():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        # The login response is shortened for test purposes
        b'%s%s OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL]\r\n' % (tagpre, b'000'),
        b'* LIST (\\HasNoChildren) "/" INBOX\r\n',
        b'* LIST (\\HasNoChildren) "/" Teste\r\n',
        b'* LIST (\\HasNoChildren) "/" Trash\r\n',
        b'%s%s OK Completed (0.044 secs 12 calls)\r\n' % (tagpre, b'001'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'002'),
    )
    imap.login('user', 'pass')
    imap.list()
    assert imap.sstatus['list_response'][0].native() == 'INBOX'
    assert imap.sstatus['list_response'][1].native() == 'Teste'
    assert imap.sstatus['list_response'][2].native() == 'Trash'


def test_command_listrights():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* LISTRIGHTS INBOX user p l r s w i k x t e c d a n 0 1 2 3 4 5 6 7 8 9\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.listrights('INBOX', 'user')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_login():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        # The login response is shortened for test purposes
        b'%s%s OK [CAPABILITY IMAP4rev1 LITERAL+ ID ENABLE ACL]\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    status, tag, _ = imap.login('user', 'pass')
    assert imap.state == 'AUTH'
    assert status == 'OK'


def test_command_login_bad():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        # The login response is shortened for test purposes
        b'%s%s NO Login failed: authentication failure\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    with pytest.raises(imaplib2.imapp.IMAP4P.Error, match='Could not login.'):
        imap.login('user', 'pass')


def test_command_lsub():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* LSUB () "/" INBOX\r\n',
        b'* LSUB () "/" &-c&AOM-o\r\n',
        b'* LSUB () "/" &IKw-$&-\r\n',
        b'* LSUB () "/" &IKw-$AA\r\n',
        b'* LSUB () "/" &T2BZfQ-&-world\r\n',
        b'* LSUB (\\HasChildren) "/" TEST\r\n',
        b'* LSUB () "/" TEST/TEST-2\r\n',
        b'* LSUB () "/" Trash\r\n',
        b'%s%s OK Completed (0.002 secs 9 calls)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.lsub('', '*')
    print('Command response:')
    print(response)
    for mailbox in response:
        print(mailbox)
    assert status == 'OK'


def test_command_myrights():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* MYRIGHTS INBOX lrswipkxtecdan\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.myrights('INBOX')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_namespace():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* NAMESPACE (("" "/")) (("Other Users/" "/")) (("Shared Folders/" "/"))\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.namespace()
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_noop():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.noop()
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_rename():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* OK rename TEST/TEST-2 TEST/TEST-TWO\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.rename('TEST/TEST-2', 'TEST/TEST-TWO')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_select():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 4 EXISTS\r\n',
        b'* 0 RECENT\r\n',
        b'* FLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen)\r\n',
        b'* OK [PERMANENTFLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen \\*)] Ok\r\n',
        b'* OK [UNSEEN 2] Ok\r\n',
        b'* OK [UIDVALIDITY 1682284751] Ok\r\n',
        b'* OK [UIDNEXT 5] Ok\r\n',
        b'* OK [HIGHESTMODSEQ 219] Ok\r\n',
        b'* OK [MAILBOXID (08746f42-4abc-425e-abab-c791533ce761)] Ok\r\n',
        b'* OK [URLMECH INTERNAL] Ok\r\n',
        b'* OK [ANNOTATIONS 65536] Ok\r\n',
        b'%s%s OK [READ-WRITE] Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    imap.select('INBOX')
    pprint(imap.sstatus['current_folder'])
    assert imap.sstatus['current_folder']['name'] == 'INBOX'
    assert not imap.sstatus['current_folder']['is_readonly']


def test_command_select_noauth():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK DUMMY Please login first\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    with pytest.raises(
        imaplib2.imapp.IMAP4P.Error, match="command b'SELECT' illegal in state 'NONAUTH'"
    ):
        imap.select('INBOX')


def test_command_search_uid():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* SEARCH 1 2 3 4\r\n',
        b'%s%s OK Completed (4 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.search('ALL')
    print('Command response:')
    print(response)
    assert response == (1, 2, 3, 4)


def test_command_search_seq():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* SEARCH 1 2 3 4\r\n',
        b'%s%s OK Completed (4 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    imap.search_seq('ALL')
    assert imap.sstatus['search_response'] == (1, 2, 3, 4)


def test_command_setacl():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.setacl('TEST/TEST-TWO', 'user_1', 'rls')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_sort_seq():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* SORT 5 6 7 9 8 11 12 10 1 2 3 4\r\n',
        b'%s%s OK Completed (12 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.sort_seq('(SUBJECT DATE)', 'UTF-8', 'ALL')
    print('Command response:')
    print(response)
    assert response == (5, 6, 7, 9, 8, 11, 12, 10, 1, 2, 3, 4)
    assert status == 'OK'


def test_command_sort_uid():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* SORT 5 6 7 31 25 35 36 33 1 2 3 4\r\n',
        b'%s%s OK Completed (12 msgs in 0.001 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.sort('(SUBJECT DATE)', 'UTF-8', 'ALL')
    print('Command response:')
    print(response)
    assert response == (5, 6, 7, 31, 25, 35, 36, 33, 1, 2, 3, 4)
    assert status == 'OK'


def test_command_status():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* STATUS "&BD8EQAQ4BDIENQRC- &BDwEOARA-" (MESSAGES 12 RECENT 0 UIDNEXT 37 UIDVALIDITY '
        b'1682284751 UNSEEN 5)',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.status(
        'привет мир', ['MESSAGES', 'RECENT', 'UIDNEXT', 'UIDVALIDITY', 'UNSEEN']
    )
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_store_seq():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* FLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen NonJunk Flag1 Flag2 $label1 $labe'
        b'l2 $label3 Flag TestFlag)\r\n',
        b'* OK [PERMANENTFLAGS (\\Answered \\Flagged \\Draft \\Deleted \\Seen NonJunk Flag1 Flag2 '
        b'$label1 $label2 $label3 Flag TestFlag \\*)] Ok\r\n',
        b'* 1 FETCH (FLAGS (\\Flagged \\Seen TestFlag))\r\n',
        b'* 2 FETCH (FLAGS (\\Answered \\Flagged NonJunk TestFlag))\r\n',
        b'* 3 FETCH (FLAGS (\\Flagged NonJunk TestFlag))\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.store_seq([1, 2, 3], '+FLAGS', ['TestFlag2', 'Teste'])
    print('Command response:')
    print(response)
    pprint(imap.sstatus['fetch_response'])
    assert status == 'OK'


def test_command_store():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* 1 FETCH (FLAGS (\\Seen TestFlag2 Teste) UID 1)\r\n',
        b'* 2 FETCH (FLAGS (\\Answered NonJunk TestFlag2 Teste) UID 2)\r\n',
        b'* 3 FETCH (FLAGS (NonJunk TestFlag2 Teste) UID 3)\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.store_uid([1, 2, 3], '-FLAGS', ['TestFlag2', 'Teste'])
    print('Command response:')
    print(response)
    pprint(imap.sstatus['fetch_response'])
    assert status == 'OK'


def test_command_subscribe():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'AUTH'
    status, tag, response = imap.subscribe('OneMailbox')
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_thread_seq():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* THREAD ((11)(12))((1 (3)(4))(2))(5 6 7)(8)(9)(10)\r\n',
        b'%s%s OK Completed (12 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.thread_seq('REFERENCES', 'UTF-8', 'ALL')
    print('Command response:')
    print(response)
    assert status == 'OK'
    assert response == [[[11], [12]], [[1, [3], [4]], [2]], [5, 6, 7], [8], [9], [10]]


def test_command_thread_uid():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* THREAD (35 36)(1 (2)(3)(4))(5 (6)(7))(25)(31)(33)\r\n',
        b'%s%s OK Completed (12 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.thread('ORDEREDSUBJECT', 'UTF-8', 'ALL')
    print('Command response:')
    print(response)
    assert status == 'OK'
    assert response == [[35, 36], [1, [2], [3], [4]], [5, [6], [7]], [25], [31], [33]]


def test_command_uid():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'* SEARCH 1 2 3 4 5 6 7 25 31 33 35 36\r\n',
        b'%s%s OK Completed (12 msgs in 0.002 secs)\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.uid('SEARCH', 'ALL')
    print('Command response:')
    print(response)
    assert status == 'OK'
    assert imap.sstatus['search_response'] == (1, 2, 3, 4, 5, 6, 7, 25, 31, 33, 35, 36)


def test_command_unselect():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.unselect()
    print('Command response:')
    print(response)
    assert status == 'OK'


def test_command_unsubscribe():
    IMAP4.open = open
    imap = IMAP4P('localhost')
    tagpre = imap._IMAP4P__IMAP4.tagpre
    imap._IMAP4P__IMAP4.file.readline.side_effect = (
        b'%s%s OK Completed\r\n' % (tagpre, b'000'),
        b'* BYE LOGOUT received\r\n',
        b'%s%s OK Completed\r\n' % (tagpre, b'001'),
    )
    imap.state = 'SELECTED'
    status, tag, response = imap.unsubscribe('OneMailbox')
    print('Command response:')
    print(response)
    assert status == 'OK'

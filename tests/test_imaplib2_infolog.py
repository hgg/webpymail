from imaplib2.infolog import InfoLog


def test_info_log_initialization():
    # Test default initialization
    info_log = InfoLog()
    assert len(info_log) == 0
    assert info_log.max_entries == 10

    # Test initialization with custom max_entries
    info_log_custom = InfoLog(max_entries=5)
    assert len(info_log_custom) == 0
    assert info_log_custom.max_entries == 5


def test_info_log_add_entry():
    info_log = InfoLog(max_entries=3)

    # Test adding entries
    info_log.add_entry('info', 'This is an information')
    assert len(info_log) == 1
    assert info_log[0] == {'type': 'INFO', 'data': 'This is an information'}

    # Test adding multiple entries
    info_log.add_entry('warning', 'This is a warning')
    info_log.add_entry('error', 'This is an error')
    assert len(info_log) == 3
    assert info_log[1] == {'type': 'WARNING', 'data': 'This is a warning'}
    assert info_log[2] == {'type': 'ERROR', 'data': 'This is an error'}

    # Test maximum entries reached
    info_log.add_entry('info', 'Another information')
    assert len(info_log) == 3
    assert info_log[0] == {'type': 'WARNING', 'data': 'This is a warning'}
    assert info_log[1] == {'type': 'ERROR', 'data': 'This is an error'}
    assert info_log[2] == {'type': 'INFO', 'data': 'Another information'}


def test_info_log_add_action():
    # Define a dummy action
    def dummy_action(etype, data):
        dummy_action.called = True

    dummy_action.called = False

    info_log = InfoLog(max_entries=3)

    # Test adding action
    info_log.add_action('info', dummy_action)
    assert len(info_log.action_list) == 1

    # Test action triggered by adding an entry
    info_log.add_entry('info', 'This is an information')
    assert dummy_action.called

    # Test adding action for non-existent type
    info_log.add_action('non-existent-type', dummy_action)
    assert len(info_log.action_list) == 2

    # Test action not triggered by adding entry of different type
    dummy_action.called = False
    info_log.add_entry('error', 'This is an error')
    assert not dummy_action.called

    # Test action triggered by adding entry of specified type
    info_log.add_entry('info', 'Another information')
    assert dummy_action.called
